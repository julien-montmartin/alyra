use std::fmt;
use std::collections::HashMap;
use num::pow;
use crypto::sha2::Sha256;
use crypto::ripemd160::Ripemd160;
use crypto::digest::Digest;


pub struct Cesar {

    shift: u8
}


impl Cesar {

    pub fn new(shift: u8) -> Self {

        Cesar { shift }
    }


    pub fn encrypt(&self, msg: &str) -> String {

        msg.chars().map(|c| {

            if c.is_ascii_alphabetic() {

                let base = if c.is_uppercase() {'A'} else {'a'} as u8;

                (((c as u8 - base + self.shift) % 26) + base) as char

            } else {

                c

            }
        }).collect::<String>()
    }

    pub fn decrypt(&self, msg: &str) -> String {

        msg.chars().map(|c| {

            if c.is_ascii_alphabetic() {

                let base = if c.is_uppercase() {'A'} else {'a'} as u8;

                (((26 + c as u8 - base - self.shift) % 26) + base) as char

            } else {

                c

            }
        }).collect::<String>()
    }
}


pub fn clear_to_vigenere(key: &str, msg: &str) -> String {

    let spin = key.chars().filter(char::is_ascii_alphabetic).flat_map(char::to_uppercase).cycle();

    msg.chars().zip(spin).fold(String::new(), |mut vgn, (c, k)| {

        if c.is_ascii_alphabetic() {

            let delta = k as u8 - 'A' as u8;

            let base = if c.is_uppercase() {'A'} else {'a'} as u8;

            vgn.push((((c as u8 - base + delta) % 26) + base) as char);

        } else {

            vgn.push(c);
        }

        vgn
    })
}


pub fn vigenere_to_clear(key: &str, msg: &str) -> String {

    let rev_key = key.chars().flat_map(char::to_uppercase).fold(String::new(), |mut rev, k| {

        if k.is_ascii_alphabetic() {

            rev.push((((26 + 'A' as u8 - k as u8) % 26) + 'A' as u8) as char);

        } else {

            rev.push(k);
        }

        rev
    });

    clear_to_vigenere(&rev_key, msg)
}


pub fn vigenere_to_groups(n: usize, msg: &str) -> Vec<String> {

    let mut groups:Vec<String> = Vec::new();

    for _ in 0..n {

        groups.push(String::new());
    }

    let mut i = 0 as usize;

    for c in msg.chars() {

        groups[i % n].push(c);
        i+=1;
    }

    groups
}


pub fn str_to_freq(msg: &str) -> HashMap<char, usize> {

    msg.chars().fold(HashMap::<char, usize>::new(), |mut freq, c| {

            if let Some(count) = freq.get(&c) {

                freq.insert(c, count+1);

            } else {

                freq.insert(c, 1);
            }

            freq
    })
}


pub fn freq_to_points(ref_frequencies: &HashMap<char, f32>, msg_frequencies: &HashMap<char, usize>) -> f32 {

    msg_frequencies.into_iter().fold(0.0, |score, (c, freq)| {

        if let Some(val) = ref_frequencies.get(c) {

            score + val * (*freq as f32)

        } else {

            score

        }
    })
}


pub fn str_to_bigram(msg: &str) -> HashMap<(char, char), usize> {

    let first = msg.chars();

    let mut second = msg.chars();
    second.next();

    second.zip(first).fold(HashMap::<(char, char), usize>::new(), |mut freq, (c1, c2)| {

        if let Some(count) = freq.get(&(c1, c2)) {

            freq.insert((c1, c2), count+1);

        } else {

            freq.insert((c1, c2), 1);
        }

        freq
    })
}


pub fn bigram_freq_to_points(ref_frequencies: &HashMap<(char, char), f32>, msg_frequencies: &HashMap<(char, char), usize>) -> f32 {

    msg_frequencies.into_iter().fold(0.0, |score, (bigram, freq)| {

        if let Some(proba) = ref_frequencies.get(bigram) {

            score + proba * (*freq as f32)

        } else {

            score

        }
    })
}


pub fn get_dcode_char_frequencies() -> HashMap<char, f32> {

    let mut map = HashMap::<char, f32>::new();

    map.insert('E', 17.3);
    map.insert('A', 8.4);
    map.insert('S', 8.1);
    map.insert('I', 7.3);
    map.insert('N', 7.1);
    map.insert('T', 7.1);
    map.insert('R', 6.6);
    map.insert('L', 6.0);
    map.insert('U', 5.7);
    map.insert('O', 5.3);
    map.insert('D', 4.2);
    map.insert('C', 3.0);
    map.insert('M', 3.0);
    map.insert('P', 3.0);
    map.insert('G', 1.3);
    map.insert('V', 1.3);
    map.insert('B', 1.1);
    map.insert('F', 1.1);
    map.insert('Q', 1.0);
    map.insert('H', 0.9);
    map.insert('X', 0.4);
    map.insert('J', 0.3);
    map.insert('Y', 0.3);
    map.insert('K', 0.1);
    map.insert('W', 0.1);
    map.insert('Z', 0.1);

    map
}


pub fn get_dcode_bigram_frequencies() -> HashMap<(char, char), f32> {

    let mut map = HashMap::<(char, char), f32>::new();

    map.insert(('E', 'S'), 3.27);
    map.insert(('L', 'E'), 2.31);
    map.insert(('D', 'E'), 2.15);
    map.insert(('R', 'E'), 2.06);
    map.insert(('E', 'N'), 2.03);
    map.insert(('E', 'R'), 1.65);
    map.insert(('N', 'T'), 1.63);
    map.insert(('T', 'E'), 1.6);
    map.insert(('O', 'N'), 1.56);
    map.insert(('E', 'T'), 1.4);
    map.insert(('S', 'E'), 1.39);
    map.insert(('E', 'L'), 1.34);
    map.insert(('L', 'A'), 1.28);
    map.insert(('O', 'U'), 1.23);
    map.insert(('A', 'N'), 1.22);
    map.insert(('N', 'E'), 1.18);
    map.insert(('A', 'I'), 1.12);
    map.insert(('Q', 'U'), 1.04);
    map.insert(('M', 'E'), 1.04);
    map.insert(('E', 'D'), 1.03);
    map.insert(('U', 'R'), 1.0);
    map.insert(('I', 'S'), 0.97);
    map.insert(('E', 'C'), 0.96);
    map.insert(('I', 'T'), 0.95);
    map.insert(('I', 'E'), 0.95);
    map.insert(('E', 'M'), 0.9);
    map.insert(('T', 'I'), 0.88);
    map.insert(('I', 'N'), 0.86);
    map.insert(('U', 'E'), 0.85);
    map.insert(('R', 'A'), 0.84);
    map.insert(('C', 'E'), 0.82);
    map.insert(('E', 'U'), 0.81);
    map.insert(('A', 'R'), 0.81);
    map.insert(('N', 'S'), 0.79);
    map.insert(('S', 'A'), 0.77);
    map.insert(('T', 'A'), 0.75);
    map.insert(('C', 'O'), 0.75);
    map.insert(('S', 'T'), 0.73);
    map.insert(('U', 'N'), 0.73);
    map.insert(('I', 'L'), 0.73);
    map.insert(('E', 'P'), 0.72);
    map.insert(('S', 'S'), 0.71);
    map.insert(('E', 'E'), 0.7);
    map.insert(('T', 'R'), 0.7);
    map.insert(('A', 'U'), 0.67);
    map.insert(('A', 'L'), 0.65);
    map.insert(('P', 'A'), 0.62);
    map.insert(('S', 'D'), 0.61);
    map.insert(('R', 'I'), 0.61);
    map.insert(('N', 'D'), 0.61);
    map.insert(('U', 'S'), 0.59);
    map.insert(('E', 'A'), 0.58);
    map.insert(('A', 'T'), 0.58);
    map.insert(('S', 'O'), 0.57);
    map.insert(('U', 'I'), 0.56);
    map.insert(('S', 'I'), 0.54);
    map.insert(('R', 'O'), 0.53);
    map.insert(('L', 'L'), 0.51);
    map.insert(('V', 'E'), 0.51);
    map.insert(('O', 'R'), 0.51);
    map.insert(('L', 'I'), 0.5);
    map.insert(('M', 'A'), 0.49);
    map.insert(('U', 'T'), 0.49);
    map.insert(('O', 'I'), 0.48);
    map.insert(('I', 'R'), 0.48);
    map.insert(('P', 'O'), 0.48);
    map.insert(('C', 'H'), 0.48);
    map.insert(('N', 'C'), 0.48);
    map.insert(('P', 'E'), 0.47);
    map.insert(('A', 'S'), 0.46);
    map.insert(('S', 'L'), 0.45);
    map.insert(('A', 'C'), 0.44);
    map.insert(('I', 'O'), 0.44);
    map.insert(('T', 'D'), 0.44);
    map.insert(('S', 'P'), 0.44);
    map.insert(('P', 'R'), 0.43);
    map.insert(('N', 'A'), 0.43);
    map.insert(('D', 'U'), 0.42);
    map.insert(('T', 'O'), 0.41);
    map.insert(('D', 'A'), 0.41);
    map.insert(('R', 'S'), 0.4);
    map.insert(('S', 'C'), 0.39);
    map.insert(('O', 'M'), 0.39);
    map.insert(('A', 'P'), 0.37);
    map.insert(('S', 'U'), 0.37);
    map.insert(('D', 'I'), 0.37);
    map.insert(('G', 'E'), 0.37);
    map.insert(('T', 'S'), 0.36);
    map.insert(('T', 'L'), 0.35);
    map.insert(('R', 'T'), 0.35);
    map.insert(('L', 'O'), 0.35);
    map.insert(('R', 'L'), 0.34);
    map.insert(('N', 'O'), 0.33);
    map.insert(('H', 'E'), 0.33);
    map.insert(('R', 'D'), 0.33);
    map.insert(('A', 'V'), 0.33);
    map.insert(('E', 'V'), 0.33);
    map.insert(('C', 'A'), 0.32);
    map.insert(('N', 'I'), 0.31);
    map.insert(('T', 'U'), 0.29);

    map
}


#[derive(PartialEq, Eq)]
pub struct EllipticCurve {

    a: i32,
    b: i32
}


impl EllipticCurve
{
    pub fn new(a: i32, b: i32) -> Self {

        if 4 * pow(a, 3) + 27 * pow(b, 2) == 0 {

            panic!("Invalid curve parameters a={} b={}", a, b);
        }

        EllipticCurve { a, b }
    }

    pub fn test(&self, x: i32, y: i32) -> bool {

        pow(x, 3) + self.a * x + self.b == pow(y, 2)
    }
}


impl fmt::Display for EllipticCurve {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {

        write!(fmt, "EllipticCurve({}, {})", self.a, self.b)
    }
}


pub fn get_bitcoin_address(key: &str) -> String {

    //https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses

    let  hex_key = hex::decode(key).unwrap();

    // Perform SHA-256 hashing on the public key

    let mut sha256_hasher = Sha256::new();
    sha256_hasher.input(&hex_key);

    let mut sha: [u8 ; 32] = [0 ; 32];
    sha256_hasher.result(&mut sha);

    // Perform RIPEMD-160 hashing on the result of SHA-256

    let mut ripemd160_hasher = Ripemd160::new();
    ripemd160_hasher.input(&sha);

    let mut rip : [u8 ; 20] = [0 ; 20];
    ripemd160_hasher.result(&mut rip);
    //let dbg_see_rip = hex::encode(rip.as_ref());

    // Add version byte in front of RIPEMD-160 hash (0x00 for Main Network)

    let main_net : [u8 ; 1] = [0];
    sha256_hasher.reset();

    // Perform SHA-256 hash on the extended RIPEMD-160 result

    sha256_hasher.input(&main_net);
    sha256_hasher.input(&rip);

    let mut sha_rip: [u8 ; 32] = [0 ; 32];
    sha256_hasher.result(&mut sha_rip);

    // Perform SHA-256 hash on the result of the previous SHA-256 hash

    sha256_hasher.reset();
    sha256_hasher.input(&sha_rip);

    let mut sha_sum: [u8 ; 32] = [0 ; 32];
    sha256_hasher.result(&mut sha_sum);

    // Take the first 4 bytes of the second SHA-256 hash. This is the address checksum

    let checksum = &sha_sum[0..4];

    // Add the 4 checksum bytes from stage at the end of extended RIPEMD-160.
    // This is the 25-byte binary Bitcoin Address.

    let address = [&main_net[..], &rip[..], &checksum[..]].concat();

    // Convert the result from a byte string into a base58 string

    bs58::encode(address).into_string()
}


#[cfg(test)]
mod tests {
    use crate::*;

    // Exercice 1.3.1 : Réaliser un chiffrement de Cesar
    #[test]
    fn cesar_1_3_1() {
        let c1 = Cesar::new(1);

        let res = c1.encrypt("abc");
        assert_eq!(res, "bcd".to_string());

        let c2 = Cesar::new(2);

        let res = c2.encrypt("abc");
        assert_eq!(res, "cde".to_string());

        let c3 = Cesar::new(3);

        let res = c3.encrypt("abc");
        assert_eq!(res, "def".to_string());

        //////////

        // Améliorer le programme pour prendre en compte les majuscules, chiffres et caractères spéciaux
        // (On ne touche pas à ce qui ne relève pas de l'alphabet latin)

        let msg = "Tiens, c'est pas bête ! 😉";

        let res = c1.encrypt(msg);
        assert_eq!(res, "Ujfot, d'ftu qbt cêuf ! 😉".to_string());

        let res = c1.decrypt(&c1.encrypt(msg));
        assert_eq!(res, msg);
    }


    // Exercice 1.3.2 : Trouver la fréquence d'une lettre dans un texte
    #[test]
    fn freq_1_3_2() {

        //frequences(“Etre contesté, c’est être constaté”)
        let freq = str_to_freq("Etre contesté, c’est être constaté");

        let table = [(7, 't'), (4, 'e'), (4, ' '), (3, 's'), (3, 'c'), (2, 'r'), (2, 'o'),
            (2, 'n'), (2, 'é'), (1, 'ê'), (1, 'E'), (1, 'a'), (1, '’'), (1, ',')];

        for (f, c) in &table {
            assert_eq!(freq[&c], *f);
        }
    }


    //Entrainement : Chiffrer et déchiffrer un message de Vigenere
    #[test]
    fn entrainement_vigenere_a_b_c() {
        let res = clear_to_vigenere("ABC", "VOICIUNMESSAGE");
        assert_eq!(res, "VPKCJWNNGSTCGF".to_string());

        let res = clear_to_vigenere("Abc", "VoiciUnMessage");
        assert_eq!(res, "VpkcjWnNgstcgf".to_string());

        let key = "ABC";
        let msg = "Tiens, c'est pas bête ! 😉";

        let res = clear_to_vigenere(key, msg);
        assert_eq!(res, "Tjgnt, d'etv qcs dêug ! 😉".to_string());

        let res = vigenere_to_clear(key, &clear_to_vigenere(key, msg));
        assert_eq!(res, msg);

        let key = "T😵t😵";
        let res = vigenere_to_clear(key, &clear_to_vigenere(key, msg));
        assert_eq!(res, msg);

        let key = " T 😵 t 😵 ";
        let res = vigenere_to_clear(key, &clear_to_vigenere(key, msg));
        assert_eq!(res, msg);

        let key = "   ";
        let res = vigenere_to_clear(key, msg);
        assert_eq!(res, "");

        let key = "";
        let res = vigenere_to_clear(key, msg);
        assert_eq!(res, "");

        let groups = vigenere_to_groups(3, "MESVIEILLESTANTES");
        assert_eq!(groups, ["MVIEAE", "EILSNS", "SELTT"]);
    }


    //Exercice 1.3.4 : La cryptographie asymétrique en détails
    #[test]
    #[should_panic(expected = "Invalid curve parameters a=0 b=0")]
    fn courbe_elliptique_1() {

        let _c = EllipticCurve::new(0, 0);
    }


    //Exercice 1.3.4 : La cryptographie asymétrique en détails
    #[test]
    fn courbe_elliptique_2() {

        let c = EllipticCurve::new(12, 64);

        let str = c.to_string();
        assert_eq!(str, "EllipticCurve(12, 64)");

        let rv = c.test(0, 8);
        assert_eq!(rv, true);

        let rv = c.test(1, 8);
        assert_eq!(rv, false);
    }


    //Exercice 1.3.5 :Génèrer une adresse type bitcoin
    #[test]
    pub fn adresse_bitcoin() {

        // Voir ici : https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses

        let address = get_bitcoin_address("0250863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b2352");
        assert_eq!(address, "1PMycacnJaSqwwJqjawXBErnLsZ7RkXUAs");
    }
}