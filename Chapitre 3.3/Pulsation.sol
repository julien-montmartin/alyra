pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";


contract Pulsation {

    using SafeMath for uint;

    uint public battement;
    string message;

	constructor(string memory m) public {

		battement = 0;
        message = m;
    }

    function ajouterBattement() public returns (string memory) {

		battement=battement.add(1);
		return message;
	}
}


contract Pendule  {

    Pulsation pulse;

	constructor() public {

		pulse = new Pulsation("tic");
	}

    function provoquerUnePulsation() public {
 
        pulse.ajouterBattement();
    }
}


contract PenduleExterne {

    string[] public balancier;
 
    Pulsation tic;
    Pulsation tac;

    function ajouterTicTac(Pulsation inTic, Pulsation inTac) public {
 
        tic = inTic;
        tac = inTac;
    }

    function mouvementsBalancier() public {
 
        balancier.push(tic.ajouterBattement());
        balancier.push(tac.ajouterBattement());
    }
}


contract PenduleTicTac {

    using SafeMath for uint;

    string[] public balancier;
 
    Pulsation tic;
    Pulsation tac;
    
    constructor() public {
    
        tic = new Pulsation("tic");
        tac = new Pulsation("tac");
    }

    function mouvementsBalancier(uint k) public {
 
        for(uint i = 0 ; i < k ; i=i.add(1)) {
    
            if(balancier.length%2 == 0) {
    
                balancier.push(tic.ajouterBattement());
    
            } else {
    
                balancier.push(tac.ajouterBattement());
            }
        }
    }
}