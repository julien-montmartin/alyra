// Le défi de cette semaine consiste à créer une place de marché décentralisée pour illustrateurs
// indépendants, avec :
//
//	- Un mécanisme de réputation
//	- Une liste de demandes et d’offres de services
//	- Un mécanisme de contractualisation avec dépôt préalable
//
//
// Mécanisme de réputation
//
//	- Pour représenter la réputation, nous allons associer chaque utilisateur à une valeur entière.
//
//	- Lorsqu’un nouveau participant rejoint la plateforme, il appelle la fonction inscription() qui
//	  lui donne une réputation de 1. Il faut une réputation minimale pour accéder à la plupart des
//	  fonctionnalités. Un nom est aussi associé à l’adresse.
//
//
// Liste de demandes
//
//	- Définir une structure de données pour chaque demande qui comprend:
//		- La rémunération (en wei)
//		- Le délai à compter de l’acceptation (en secondes)
//		- Une description de la tâche à mener (champ texte)
//		- L’état de la demande : OUVERTE, ENCOURS, FERMEE (avec une énumération)
//		- Définir une réputation minimum pour pouvoir postuler
//		- Une liste de candidats
//
// 	- Créer un tableau des demandes
//
// 	- Créer une fonction ajouterDemande() qui permet à une entreprise de formuler une
// 	  demande. L’adresse du demandeur doit être inscrite sur la plateforme. L’entreprise doit en
// 	  même temps déposer l’argent sur la plateforme correspondant à la rémunération + 2% de frais
// 	  pour la plateforme.
//
//	- Ecrire l’interface qui permet de lister ces offres
//
//
// Mécanisme de contractualisation
//
//	- Créer une fonction postuler() qui permet à un indépendant de proposer ses services. Il est
//	  alors ajouté à la liste des candidats
//
//  - Créer une fonction accepterOffre() qui permet à l’entreprise d’accepter un illustrateur. La
//    demande est alors ENCOURS jusqu’à sa remise
//
//   - Ecrire une fonction livraison() qui permet à l’illustrateur de remettre le hash du lien où
//     se trouve son travail. Les fonds sont alors automatiquement débloqués et peuvent être
//     retirés par l’illustrateur. L’illustrateur gagne aussi un point de réputation


pragma solidity >=0.5.7;

// Guide officiel pour le style
// https://solidity.readthedocs.io/en/v0.5.10/style-guide.html

// Fonctions arithmétiques sûres
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

// Pour pouvoir retourner une struct depuis une fonction
pragma experimental ABIEncoderV2;


/// @title	Alyra défi 2 - Place de marché pour illustrateurs
/// @author Julien M.
contract PlaceDeMarcheIllustrateurs {

    using SafeMath for uint256;

    address payable administrateur;    // encaisse les frais et clôt le contrat    

	struct Illustrateur {

		string nom;			// "Prénom Nom" de l'illustrateur
		uint8 reputation; 	// Réputation (bornée à 100) de l'illustrateur
	}

	mapping (address => Illustrateur) annuaireIllustrateurs;

    event InscriptionIllustrateur(address illustrateur, string nom);


	struct Demande {

		uint256 remuneration;	// en wei
		uint32 delais;			// en s à partir de l'acceptation
		string description;		// description de la tâche à effectuer
		uint8 reputation;		// réputation minimum pour postuler
		address demandeur;		// adresse de l'entreprise
	}
	
	uint32 constant MIN_DESCRIPTION = 32;   // longueur minimale (en octets) d'une description
    uint256 constant MIN_FRAIS = 2000;      // les frais minimum de la plateforme en wei
     
	mapping (bytes32 => Demande) demandes;
	bytes32[] hashDemandes;


	enum StatusDemande {OUVERTE, ENCOURS, FERMEE}

	struct AvancementDemande {

		bytes32 hashDemande;	// le hash keccak256 de la Demande
		StatusDemande status;	// l'état d'avancement de la demande
		address[] candidats;	// liste des candidats qui ont postulé
	}

	mapping(bytes32 => AvancementDemande) avancementDemandes;

    event AjoutNouvelleDemande(address demandeur, bytes32 demande);
    event Candidature(bytes32 demande, address illustrateur);


	mapping (bytes32 => string) travaux;
	bytes32[] hashTravaux;

	mapping (address => bytes32[]) portfolioIllustrateur;


    constructor(address payable admin) public {

        administrateur = admin;
    }
    

	/// Retourne True ou False selon que l'illustrateur est valide ou non
    /// @param illustrateur l'illustrateur dont on veut vérifier la validité
    
	function illustrateurValide(Illustrateur memory illustrateur) public pure returns (bool) {
	    
	    return bytes(illustrateur.nom).length > 0 && illustrateur.reputation > 0;
	}
	

	/// Inscrit un nouvel illustrateur, et lui donne une réputation de 1
	/// @param nomIllustrateur le "Prénom Nom" de l'utilisateur
	function inscription(string memory nomIllustrateur) public {

        bytes memory bytesIllustrateur = bytes(nomIllustrateur);

        require(bytesIllustrateur.length > 0);
        
        address adresseIllustrateur = msg.sender;
        
        require(bytes(annuaireIllustrateurs[adresseIllustrateur].nom).length == 0);
        
        annuaireIllustrateurs[adresseIllustrateur] = Illustrateur(nomIllustrateur, 1 /*réputation initiale*/);

        emit InscriptionIllustrateur(adresseIllustrateur, nomIllustrateur);     
	}
	
	
	/// Retourne True ou False selon que la demande est enregistrée ou non
    /// @param hashDemande le hash de la demande dont on veut vérifier l'existence
    
	function demandeExiste(bytes32 hashDemande) public view returns (bool) {
	    
	    return demandes[hashDemande].remuneration > 0;
	}
	
	
	/// Retourne True ou False selon que la demande est enregistrée ou non
    /// @param demande le hash de la demande dont on veut vérifier l'existence
    
	function demandeValide(Demande memory demande) public pure returns (bool) {
	    
	    return demande.delais > 0 && bytes(demande.description).length >= MIN_DESCRIPTION && demande.reputation > 0;
	}


	/// Ajoute la nouvelle demande d'une entreprise, doit s'accompagner d'un paiement de
	/// 'rémunération+2%' pour les frais de la place de marché
	/// @param remuneration rémunération de la tâche en wei
	/// @param delais délais de l'illustrateur en s à partir de l'acceptation par l'entreprise
	/// @param description description de la tâche à effectuer par l'illustrateur
	/// @param reputation réputation minimum de l'illustrateur pour postuler

	function ajouterDemande(

		uint256 remuneration,
		uint32 delais,
		string memory description,
		uint8 reputation
	)
		public payable
	{
        uint256 frais = remuneration.mul(2).div(100);
        
        // Une demande peut-être non rémunérée pour l'illustrateur, mais pas pour la place de marché !
        if (frais < MIN_FRAIS) {
            
            frais = MIN_FRAIS;
        }
        
        uint256 coutTotal = remuneration.add(frais);
        require(msg.value >= coutTotal);
        
        administrateur.transfer(frais); // l'admin du contrat encaisse les frais
        
        address adresseDemandeur = msg.sender;
        
        Demande memory nouvelleDemande = Demande(remuneration, delais, description, reputation, adresseDemandeur);
        require(demandeValide(nouvelleDemande));

        bytes memory tmp = abi.encodePacked(remuneration, delais, description, reputation, adresseDemandeur);
        bytes32 hashNouvelleDemande = keccak256(bytes(tmp));
        
        // Une demande identique ne doit pas déjà exister (simplifie un peu la gestion)
        require(!demandeExiste(hashNouvelleDemande));
        
        demandes[hashNouvelleDemande] = nouvelleDemande;
    	hashDemandes.push(hashNouvelleDemande);
        
        AvancementDemande memory avancementNouvelleDemande = AvancementDemande(
                hashNouvelleDemande,
                StatusDemande.OUVERTE,
                new address[](0));
        avancementDemandes[hashNouvelleDemande] = avancementNouvelleDemande;

        emit AjoutNouvelleDemande(adresseDemandeur, hashNouvelleDemande);
	}


	/// Liste les demandes des entreprises

	function listerDemandes() public view returns (AvancementDemande[] memory) {

        uint256 n = hashDemandes.length;

        AvancementDemande[] memory listeDemandes = new AvancementDemande[](n);
        
        for (uint256 i=0 ; i<n ; i++) {
            
            bytes32 h = hashDemandes[i];
            AvancementDemande memory d = avancementDemandes[h];
            listeDemandes[i] = d;
        }

        return listeDemandes;
	}
	
	
	/// Lire le détail d'une demande connaissant son hash. Fonctionne avec listerDemandes().
	/// @param hashDemande le hash de la demande dont on veut connaitre le détail
	
	function lireDemande(bytes32 hashDemande) public view returns (Demande memory) {

        Demande memory demande = demandes[hashDemande];

        require(demandeValide(demande));

        return demande;
	}
	

	/// Permet à un illustrateur de proposer ses services. Il est ajouté à la liste des candidats
	/// @param hashDemande le keccak256 de la demande visée

	function postuler(bytes32 hashDemande) public {

        Demande memory demande = demandes[hashDemande];
        require(demandeValide(demande));
        
        address adresseIllustrateur = msg.sender;
        Illustrateur memory illustrateur = annuaireIllustrateurs[adresseIllustrateur];
        require(illustrateurValide(illustrateur));
        
        avancementDemandes[hashDemande].candidats.push(adresseIllustrateur);
        
        emit Candidature(hashDemande, adresseIllustrateur);
	}


	/// Permet à l’entreprise d’accepter un illustrateur. La demande est alors ENCOURS.
	/// @param illustrateur l'address de l'illustrateur

	function accepterOffre(address illustrateur) public {
	    
	    // TODO !
	}


	/// Permet à l’illustrateur de remettre le lien où trouve son travail. Les fonds sont
	/// débloqués et peuvent être retirés par l’illustrateur, qui gagne aussi un point de
	/// réputation
	/// @param hashDemande le keccak256 de la demande visée
	/// @param url le lien vers le travail de l'illustrateur

	function livraison(bytes32 hashDemande, string memory url) public {

        // TODO !
	}
}
