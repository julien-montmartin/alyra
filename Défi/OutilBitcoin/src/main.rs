use ProtocoleBitcoin::*;
use std::io::{self, Read};

#[macro_use]
extern crate clap;
use clap::App;

fn main() {

    // The YAML file is found relative to the current file, similar to how modules are found
    let yaml = load_yaml!("cli.yml");
    let app =  App::from_yaml(yaml);
    let matches = app.get_matches();


    if let Some(values) =  matches.values_of("int-to-varint") {

        let ints: Vec<_> = values.collect();

        for i in ints {

            if let Ok(i) = i.parse() {

                let (_, _, v) = u64_to_hex(i);
                println!("int {} ⇝ varint {}", i, v);
            }
        }
    }


    if let Some(values) =  matches.values_of("varint-to-int") {

        let varints: Vec<_> = values.collect();

        for v in varints {

            let bytes= hex::decode(v).unwrap();
            let mut buf = io::Cursor::new(&bytes[..]);

            if let Ok(i) = parse_varint(&mut buf) {

                println!("varint {} ⇝ int {}", v, i);
            }
        }
    }


    if let Some(values) =  matches.values_of("transaction") {

        let transactions: Vec<_> = values.collect();

        for t in transactions {

            let bytes= hex::decode(t).unwrap();
            let mut buf = io::Cursor::new(&bytes[..]);

            if let Ok(t) = parse_transaction(&mut buf) {

                println!("Parse <transaction data> ⇝ {}", t);
            }
        }
    }


    if let Some(values) =  matches.values_of("p2pkh") {

        let p2pkhs: Vec<_> = values.collect();

        for p in p2pkhs {

            let bytes= hex::decode(p).unwrap();
            let mut buf = io::Cursor::new(&bytes[..]);

            let mut stack = Vec::new();

            println!("Run pay to pubkey hash <script sig + pub key>  ⇝");

            loop {

                let res = OpCode::exec(&mut buf, &mut stack);

                match res {

                    Ok(OpCode::OP_EQUALVERIFY(false)) => { println!("Transaction is invalid"); break }

                    Ok(op) => { println!("{:?} ok", op) }

                    Err(ref e) if e.kind() == io::ErrorKind::UnexpectedEof => { println!("Done"); break }

                    Err(e) => { println!("Fail with {}", e); break }
                }

                print_stack(&stack);
                println!("");
            }
        }
    }
}

/*
Lancé avec :

OutilBitcoin
--int-to-varint 106
--int-to-varint 550
-i 998000
--varint-to-int 6a
--varint-to-int fd2602
-v fe703a0f00
--transaction 0100000001f129de033c57582efb464e94ad438fff493cc4de4481729b85971236858275c2010000006a4730440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c62d2c29ff6863d501affffffff02ccaec817000000001976a9142527ce7f0300330012d6f97672d9acb5130ec4f888ac18411a000000000017a9140b8372dffcb39943c7bfca84f9c40763b8fa9a068700000000
--p2pkh 483045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001210372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c76a9147c3f2e0e3f3ec87981f9f2059537a355db03f9e888ac


Le programme produit la sortie suivante :

int 106 ⇝ varint 6a
int 550 ⇝ varint fd2602
int 998000 ⇝ varint fe703a0f00
varint 6a ⇝ int 106
varint fd2602 ⇝ int 550
varint fe703a0f00 ⇝ int 998000
Parse <transaction data> ⇝ Transaction
- version: 1
- flags: None

input:
- prev_tx_hash: c2758285361297859b728144dec43c49ff8f43ad944e46fb2e58573c03de29f1
- prev_tx_out_index: 1
- tx_in_script: 4730440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c62d2c29ff6863d501a
- sequence: 4294967295

output:
- value: 399027916
- tx_out_script: 76a9142527ce7f0300330012d6f97672d9acb5130ec4f888ac

output:
- value: 1720600
- tx_out_script: a9140b8372dffcb39943c7bfca84f9c40763b8fa9a0687

lock_time: 0

Run pay to pubkey hash <script sig + pub key>  ⇝
PASSTHRU ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001

PASSTHRU ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c

OP_DUP ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
2: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c

OP_HASH160 ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
2: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8

PASSTHRU ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
2: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8
3: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8

OP_EQUALVERIFY(true) ok
0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c

OP_CHECKSIG ok
0: len=1 data=01

Done

Process finished with exit code 0
*/