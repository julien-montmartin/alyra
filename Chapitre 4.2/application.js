async function createMetaMaskDapp() {

	try {

		// Demande à MetaMask l'autorisation de se connecter
		const addresses = await ethereum.enable();
		const address = addresses[0];

		// Connection au noeud fourni par l'objet web3
		const provider = new ethers.providers.Web3Provider(ethereum);

		dapp = { address, provider };
		console.log(dapp);

	} catch(err) {

		// Gestion des erreurs
		console.error(err);
	}
}


async function getBalance(){

	let label = 'Solde : ';

	let item = document.getElementById('balance');
	item.innerHTML = label;

	dapp.provider.getBalance(dapp.address).then((balance) => {

		let etherBalance = ethers.utils.formatEther(balance);
		let txt = label + etherBalance;

		console.log(txt);
		item.innerHTML = txt;
	});
}


async function getLastBlock(){

	let label = 'Dernier bloc : ';

	let item = document.getElementById('block')
	item.innerHTML = label;

	dapp.provider.getBlockNumber().then((number) => {

		let txt = label + number;

		console.log(txt);
		item.innerHTML = txt;
	});
}


async function getGasPrice(){

	let label = 'Prix du gaz : ';

	let item = document.getElementById('gas');
	item.innerHTML = label;

	dapp.provider.getGasPrice().then((price) => {

		let etherGas = ethers.utils.formatEther(price);
		let txt = label + etherGas;

		console.log(txt);
		item.innerHTML = txt;
	});
}


const credibility = {

	address : "0xb4F798B5CfD5Ed6f41b863eA1a34879eCCD04eA4",

	abi : [
		{
			"constant": false,
			"inputs": [
				{
					"name": "dev",
					"type": "bytes32"
				}
			],
			"name": "remettre",
			"outputs": [
				{
					"name": "",
					"type": "uint256"
				}
			],
			"payable": false,
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"name": "",
					"type": "address"
				}
			],
			"name": "cred",
			"outputs": [
				{
					"name": "",
					"type": "uint256"
				}
			],
			"payable": false,
			"stateMutability": "view",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"name": "url",
					"type": "string"
				}
			],
			"name": "produireHash",
			"outputs": [
				{
					"name": "",
					"type": "bytes32"
				}
			],
			"payable": false,
			"stateMutability": "pure",
			"type": "function"
		},
		{
			"constant": false,
			"inputs": [
				{
					"name": "destinataire",
					"type": "address"
				},
				{
					"name": "valeur",
					"type": "uint256"
				}
			],
			"name": "transfer",
			"outputs": [],
			"payable": false,
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"name": "payeur",
					"type": "address"
				},
				{
					"indexed": false,
					"name": "destinataire",
					"type": "address"
				},
				{
					"indexed": false,
					"name": "montant",
					"type": "uint256"
				}
			],
			"name": "Transfer",
			"type": "event"
		},
		{
			"anonymous": false,
			"inputs": [
				{
					"indexed": false,
					"name": "dev",
					"type": "bytes32"
				},
				{
					"indexed": false,
					"name": "adr",
					"type": "address"
				}
			],
			"name": "Devoir",
			"type": "event"
		}
	]
}


async function getMyCredibility(){

	let label = 'Crédibilité : ';

	let item = document.getElementById('credibility');
	item.innerHTML = label;

	let credibilityContract = new ethers.Contract(credibility.address, credibility.abi, dapp.provider);

	credibilityContract.cred(dapp.address).then((myCredibility) => {

		let txt = label + myCredibility;

		console.log(txt);
		item.innerHTML = txt;
	});
}


async function sendHomework(){

	let credibilityContract = new ethers.Contract(credibility.address, credibility.abi, dapp.provider.getSigner());

	credibilityContract.on('Devoir', (hash, emetteur) => {

		document.getElementById('EventHash').innerHTML = 'Devoir : ' + hash;
		document.getElementById('EventAddress').innerHTML = 'Emetteur : ' + emetteur;
	});

	let homeworkUrl = document.getElementById('homework').value;

	let label = 'Condensat : ';

	let item = document.getElementById('hash');
	item.innerHTML = label;

	credibilityContract.produireHash(homeworkUrl).then((urlHash) => {

		let txt = label + urlHash;

		console.log(txt);
		item.innerHTML = txt;

		let label2 = 'Position : ';

		let item2 = document.getElementById('position');
		item2.innerHTML = label2;

		credibilityContract.remettre(urlHash).then((myPosition) => {

			let txt2 = label2 + myPosition.value.toString();

			console.log(txt2);
			item2.innerHTML = txt2;
		});
	});
}
