import math


def calculerDifficulte_2_1_1(cible):

    cible_max = 0xffff0000000000000000000000000000000000000000000000000000

    return cible_max/cible


def calculerDifficulte_2_1_2(bits):

    exp = int(bits[2:4], 16)
    pad = exp * 2
    cible = int(bits[4:10].ljust(pad, '0'), 16)

    return calculerDifficulte_2_1_1(cible)


def blocReajustement(height):

    return height % 2016 == 0


def recompenseBloc(hauteurBloc):

    étapes = math.floor(hauteurBloc / 210000)

    diviseur = math.pow(2, étapes)

    valeur = 50.0 / diviseur

    if valeur < 1E-8:

        valeur = 0.0

    return math.floor(valeur*1E8)/1E8


def bitcoinsEnCirculation(hauteurBloc):

    somme = 0.0
    cache = 0.0

    for b in range(hauteurBloc + 1):

        if b % 210000 == 0:

            cache = recompenseBloc(b)

        if cache < 1E-8:

            break

        somme += cache

    # TODO - Comprendre ces soucis d'arrondis. Tout mettre en Satoshis ?
    if somme > 20999999.9769:

        somme = 20999999.9769

    return math.floor(somme*1E8)/1E8


# Exercice 2.1.1 : Convertir la cible en difficulté (Python 3)

d = calculerDifficulte_2_1_1(1147152896345386682952518188670047452875537662186691235300769792000)
print("Difficulté : %f" % (d))  #Difficulté : 23.501257


# Exercice 2.1.2 : Identifier le niveau de difficulté d'un bloc (Python 3)

d = calculerDifficulte_2_1_2('0x1c0ae493')
print("Difficulté : %f" % (d))  #Difficulté : 23.501257

d = calculerDifficulte_2_1_2('0x1d00ffff')
print("Difficulté : %f" % (d))  #Difficulté : 1.000000

d = calculerDifficulte_2_1_2('0x03000001')
print("Difficulté : %f" % (d))  #Difficulté : 26959535291011309493156476344723991336010898738574164086137773096960.000000

d = calculerDifficulte_2_1_2('0x1b0404cb')
print("Difficulté : %f" % (d))  #Difficulté : 16307.420939


# Exercice 2.1.3 : Vérifier si la difficulté a été ajustée

r = blocReajustement(556415)
print("Réajustement 556415 : %r" % (r)) #False

r = blocReajustement(556416)
print("Réajustement 556416 : %r" % (r)) #True


# Exercice 2.1.5 : Calculer la récompense associée à un bloc (Python 3)

v = recompenseBloc(0)
print("Récompense pour hauteur 0 : %.10f" % (v))       #50

v = recompenseBloc(1)
print("Récompense pour hauteur 1 : %.10f" % (v))       #50

v = recompenseBloc(6929998)
print("Récompense pour hauteur 6929998 : %.10f" % (v))  #0.00000001


# Exercice 2.1.6 : Calculer le nombre total de Bitcoin à un instant t (Python 3)

s = bitcoinsEnCirculation(0)
print("Total bitcoins pour hauteur 0 : %.10f" % (s))  #50.0

s = bitcoinsEnCirculation(210000)
print("Total bitcoins pour hauteur 210000 : %.10f" % (s))  #10500025.0

s = bitcoinsEnCirculation(2100001)
print("Total bitcoins pour hauteur 2100001 : %.10f" % (s))  #20979492.28515624
