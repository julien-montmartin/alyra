pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";


contract Cogere {

    using SafeMath for uint;

	mapping (address => uint) organisateurs;

	uint partsRestantes;


	constructor() public {

		partsRestantes=100;
	}


	function transfererOrga(address orga, uint parts) public {

		require(orga != msg.sender);

		organisateurs[msg.sender] = organisateurs[msg.sender].sub(parts);
		organisateurs[orga] = organisateurs[orga].add(parts);
	}


	function estOrga(address orga) public view returns (bool){

		return organisateurs[orga] > 0;
	}


	function partsOrga(address orga) public view returns (uint){

		return organisateurs[orga];
	}


	function valeurPart() public view returns (uint) {

		return address(this).balance.div(partsRestantes);
	}


	function retraitOrga(address payable orga) public {

		uint nbParts = partsOrga(orga);

		require(nbParts > 0);

		if(partsRestantes > nbParts) {

			msg.sender.transfer(valeurPart().mul(nbParts));

			partsRestantes = partsRestantes.sub(nbParts);

			delete organisateurs[orga];

		} else {

			selfdestruct(orga);
		}
	}
}


contract CagnotteFestival is Cogere {

	uint placesRestantes;

	uint private depensesTotales;

	uint private seuilDepenseJournaliere;

	uint dateFestival;
	uint dateLiquidation;

	mapping (address => bool) festivaliers;

	mapping (address => string) sponsors;

	mapping (uint => uint) depensesJournalieres;


	constructor(uint date, uint seuil) public {

		organisateurs[msg.sender] = 100;

		dateFestival = date;
		dateLiquidation = dateFestival + 2 weeks;

		seuilDepenseJournaliere = seuil;
	}


	function acheterTicket() public payable {

		require(msg.value >= 500 finney, "Place à 0.5 Ethers");

		placesRestantes = placesRestantes.sub(1);
		festivaliers[msg.sender] = true;
	}


	function payer(address payable destinataire, uint montant) public {

		require(estOrga(msg.sender));
		require(destinataire != address(0));
		require(montant > 0);

		destinataire.transfer(montant);
	}


	function controlerDepense(uint montant) internal view returns (bool) {

		return depensesJournalieres[now].add(montant) <= seuilDepenseJournaliere;
	}


	function comptabiliserDepense(uint montant) private {

		require(controlerDepense(montant));

		depensesJournalieres[now] = depensesJournalieres[now].add(montant);
		depensesTotales = depensesTotales.add(montant);
	}


	function retraitOrga() public {

		require(now >= dateLiquidation);

		super.retraitOrga(msg.sender);
	}


	function () external payable {

		//vide
	}


	function sponsoriser(string memory nom) public payable {

		require(msg.value >= 30 ether);
		require(bytes(sponsors[msg.sender]).length == 0);

		sponsors[msg.sender] = nom;
	}
 }
