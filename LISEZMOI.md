![Alyra](https://bitbucket.org/julien-montmartin/alyra/raw/08e2559c1fd366c7c48b6e561e61cfc460d3db53/Alyra.png)

# Le code de la formation

En Rust, Python, JavaScript ou Solidity

## Bitcoin et fondamentaux

- Exercice 1.1.1: Trouver un nombre aléatoire ⇝ [NombreAléatoire.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.1/NombreAl%C3%A9atoire.py)
- Exercice 1.1.2 : Trouver un nombre saisi par l'utilisateur efficacement ⇝ [Dichotomie.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.1/Dichotomie.py)
- Exercice 1.1.3 : Réaliser un vérificateur de palindrome (JavaScript) ⇝ [Palindrome.js](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.1/Palindrome.js)
- Exercice 1.1.3 : Réaliser un vérificateur de palindrome (Python 3) ⇝ [Palindrome.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.1/Palindrome.py)
- Exercice 1.1.4 : Récupérer l'aire et le périmètre d'un cercle ⇝ [Cercle.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.1/Cercle.py)
- Exercice 1.2.1 : Calculer la factorielle d'un nombre ⇝ [Factorielle.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.2/Factorielle.py)
- Exercice 1.2.2 : Obtenir les meilleurs pourboires ⇝ [SacÀDos.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.2/Sac%C3%80Dos.py)
- Exercice 1.2.5 : Explorer un arbre ⇝ [ExplorerUnArbre.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.2/ExplorerUnArbre.py)
- Exercice 1.2.7 : Établir une table de hachage ⇝ [ÉtablirUneTableDeCondensats.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.2/%C3%89tablirUneTableDeCondensats.py)
- Exercice 1.2.8 : Créer un arbre de Merkle ⇝ voir `merkle_tree()` dans [Merkle/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.2/Merkle/src/lib.rs)
- Exercice 1.3.1 : Réaliser un chiffrement de Cesar ⇝ voir `cesar_1_3_1()` dans [NotionsDeCryptographie/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/NotionsDeCryptographie/src/lib.rs)
- Exercice 1.3.2 : Trouver la fréquence d'une lettre dans un texte ⇝ voir `freq_1_3_2()` dans [NotionsDeCryptographie/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/NotionsDeCryptographie/src/lib.rs)
- Entrainement : Chiffrer et déchiffrer un message de Vigenere (a, b, c) ⇝ voir `entrainement_vigenere_a_b_c()` dans [NotionsDeCryptographie/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/NotionsDeCryptographie/src/lib.rs)
- Entrainement : Chiffrer et déchiffrer un message de Vigenere (d) ⇝ voir `main()` dans [vigenere/src/main.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/vigenere/src/main.rs)
- Exercice 1.3.4 : La cryptographie asymétrique en détails ⇝ voir `courbe_elliptique_1&2()` dans [NotionsDeCryptographie/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/NotionsDeCryptographie/src/lib.rs)
- Exercice 1.3.5 : Génèrer une adresse type bitcoin ⇝ voir `adresse_bitcoin()` dans [NotionsDeCryptographie/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.3/NotionsDeCryptographie/src/lib.rs)
- Exercice 1.4.1 et 1.4.2 : Convertir un nombre décimal en hexadécimal ⇝ voir `decimal_vers_hexadecimal()` dans [ProtocoleBitcoin/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.4/ProtocoleBitcoin/src/lib.rs)
- Exercice 1.4.3 : écrire un script (qui décode une entrée) ⇝ voir `parser_entree()` dans [ProtocoleBitcoin/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.4/ProtocoleBitcoin/src/lib.rs)
- Exercice 1.4.4 : écrire un script (qui décode la transaction) ⇝ voir `parser_transaction()` dans [ProtocoleBitcoin/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.4/ProtocoleBitcoin/src/lib.rs)
- Exercice 1.4.7 : Vérifier la validité d’une transaction Pay-to-pubkey-hash ⇝ voir `verifier_p2pkh()` dans [ProtocoleBitcoin/src/lib.rs](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%201.4/ProtocoleBitcoin/src/lib.rs)

## Bitcoin Avancé

- Exercice 2.1.1 : Convertir la cible en difficulté (Python 3) ⇝ [ÉvolutionsBitcoin.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%202.1/%C3%89volutionsBitcoin.py)
- Exercice 2.1.2 : Identifier le niveau de difficulté d'un bloc (Python 3) ⇝ [ÉvolutionsBitcoin.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%202.1/%C3%89volutionsBitcoin.py)
- Exercice 2.1.3 : Vérifier si la difficulté a été ajustée ⇝ [ÉvolutionsBitcoin.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%202.1/%C3%89volutionsBitcoin.py)
- Exercice 2.1.5 :  Calculer la récompense associée à un bloc ⇝ [ÉvolutionsBitcoin.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%202.1/%C3%89volutionsBitcoin.py)
- Exercice 2.1.6 : Calculer le nombre total de Bitcoin à un instant t ⇝ [ÉvolutionsBitcoin.py](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%202.1/%C3%89volutionsBitcoin.py)

## Développement de Smart Contracts

- Exercice 3.1.1 : Passer à l’artiste suivant  ⇝ [Artistes.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.1/Artistes.sol)
- Exercice 3.1.2 : Vérifier que le participant est membre ⇝ [Assemblée.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.1/Assembl%C3%A9e.sol)
- Exercice 3.1.3 : Voter et comptabiliser ⇝ [Assemblée.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.1/Assembl%C3%A9e.sol)
- Exercice 3.1.4 : Contrôle des votes ⇝ [Assemblée.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.1/Assembl%C3%A9e.sol)
- Exercice 3.1.5 : Les administrateurs ⇝ [Assemblée.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.1/Assembl%C3%A9e.sol)
- Exercice 3.2.1 : Donner le statut d'organisateurs ⇝ [Festival.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.2/Festival.sol)
- Exercice 3.2.2 : Ajouter des sponsors à une liste ⇝ [Festival.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.2/Festival.sol)
- Exercice 3.2.3 : Importer la librairie SafeMath ⇝ [Festival.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.2/Festival.sol)
- Exercice 3.2.4 : Gérer le temps dans le contrat du festival ⇝ [Festival.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.2/Festival.sol)
- Exercice 3.3.1 : Pulsation ⇝ Voir `Pulsation` dans [Pulsation.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.3/Pulsation.sol)
- Exercice 3.3.2 : Le son Tic ⇝ Voir `Pendule` dans [Pulsation.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.3/Pulsation.sol)
- Exercice 3.3.3 : Tic Tac externe ⇝ Voir `PenduleExterne` dans [Pulsation.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.3/Pulsation.sol)
- Exercice 3.3.4 : Mouvements de balanciers ⇝ Voir `PenduleTicTac` dans [Pulsation.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%203.3/Pulsation.sol)

## Interfaces des smart contracts

- Exercice 4.1.1 : Rédiger un contrat Crédibilité pour remettre des devoirs ⇝ Voir `Credibilite` dans [Credibilite.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%204.1/Credibilite.sol)
- Exercice 4.2.1 : Lire des informations d’Ethereum avec ethers.js ⇝ Voir [Chapitre 4.2](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%204.2/)
- Exercice 4.2.2 : Interagir en JavaScript avec un contrat ⇝ Voir [Chapitre 4.2](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%204.2/)
Exercice 4.2.3 : Les événements ⇝ Voir [Chapitre 4.2](https://bitbucket.org/julien-montmartin/alyra/src/master/Chapitre%204.2/)

## Défis

- Outil Bitcoin ⇝ Voir [OutilBitcoin](https://bitbucket.org/julien-montmartin/alyra/src/master/Défi/OutilBitcoin/src/)
- Place de marché ⇝ Voir [Place-de-marché.sol](https://bitbucket.org/julien-montmartin/alyra/src/master/Défi/PlaceDeMarché/Place-de-marché.sol) (pas terminé, manquent quelques lignes de Solidity, les TU et le JS)
