use byteorder::{ReadBytesExt, WriteBytesExt, LittleEndian};
use std::io::{self, Read};
use std::fmt;
use crypto::sha2::Sha256;
use crypto::ripemd160::Ripemd160;
use crypto::digest::Digest;


pub fn u64_to_hex(n: u64) -> (String, String, String) {

    let mut buf = Vec::<u8>::with_capacity(4);

    buf.write_u64::<LittleEndian>(n).unwrap();   //Sur CPU x86_64

    let mut little = String::with_capacity(8);

    for b in &buf {

            little.push_str(&format!("{:02x}", b));
    }

    let mut big = String::with_capacity(8);

    for b in buf.iter().rev() {

        big.push_str(&format!("{:02x}", b));

    }

    // http://learnmeabitcoin.com/glossary/varint

    let mut var = String::new();

    if n <= 0xfc {

        // No prefix, 1 byte
        var.push_str(&little[0..2]);

    } else if n <= 0xffff {

        // Prefix with fd, then 2 bytes
        var.push_str("fd");
        var.push_str(&little[0..4]);

    } else if n <= 0xffffffff {

        // Prefix with fe, then 4 bytes
        var.push_str("fe");
        var.push_str(&little[0..8]);

    } else {

        // Prefix with ff, then 8 bytes
        var.push_str("ff");
        var.push_str(&little[0..16]);
    }

    (little, big, var)
}


#[derive(Default, Debug)]
pub struct Transaction {

    version: u32,
    flags: Option<u8>,
    inputs: Vec<Input>,
    outputs: Vec<Output>,
    lock_time: u32
}


#[derive(Default, Debug)]
pub struct Input {

    prev_tx_hash: [u8 ; 32],
    prev_tx_out_index: u32,
    tx_in_script: Vec<u8>,
    sequence: u32
}


#[derive(Default, Debug)]
pub struct Output {

    value: u64,
    tx_out_script: Vec<u8>
}


pub fn parse_transaction(buf: &mut io::Cursor<&[u8]>) -> io::Result<Transaction> {

    //https://en.bitcoin.it/wiki/Transaction

    let mut transaction: Transaction = Default::default();

    transaction.version = buf.read_u32::<LittleEndian>()?;

    let flag_or_varint = parse_varint(buf)?;

    // Si on lit un varint qui vaut 0, c'est qu'on est en présence du champ flag optionel de deux octets. Il faut
    // encore lire ce deuxième octet, puis (re)lire le nombre d'entrées.
    let inputs_count = if flag_or_varint == 0 {

        transaction.flags = Some(buf.read_u8()?);

        parse_varint(buf)?

    } else {

        flag_or_varint
    };

    for _i in 0..inputs_count {

        transaction.inputs.push(parse_input(buf)?);
    }

    let outputs_count = parse_varint(buf)?;

    for _i in 0..outputs_count {

        transaction.outputs.push(parse_output(buf)?);
    }

    if let Some(flag) = transaction.flags {

        panic!("Segregated Witness are not handled (flags = {})", flag);
    }

    transaction.lock_time = buf.read_u32::<LittleEndian>()?;

    Ok(transaction)
}


pub fn parse_varint(buf: &mut io::Cursor<&[u8]>)-> io::Result<u64> {

    let first = buf.read_u8()?;

    let rv = if first <= 0xfc {

        // No prefix, 1 byte
        first as u64

    } else if first == 0xfd {

        // Prefix with fd, then 2 bytes
        let res = buf.read_u16::<LittleEndian>()?;
        res as u64

    } else if first == 0xfe {

        // Prefix with fe, then 4 bytes
        let res = buf.read_u32::<LittleEndian>()?;
        res as u64

    } else {

        // Prefix with ff, then 8 bytes
        let res = buf.read_u64::<LittleEndian>()?;
        res as u64
    };

    Ok(rv)
}


pub fn parse_input(buf: &mut io::Cursor<&[u8]>) -> io::Result<Input> {

    let mut input: Input = Default::default();

    buf.read_exact(&mut input.prev_tx_hash)?;

    // "Due to historical accident, the tx and block hashes that bitcoin core uses are byte-reversed."
    for i in 0..16 {

        let tmp = input.prev_tx_hash[i];
        input.prev_tx_hash[i] = input.prev_tx_hash[31-i];
        input.prev_tx_hash[31-i] = tmp;
    }

    input.prev_tx_out_index = buf.read_u32::<LittleEndian>()?;

    let script_len = parse_varint(buf)?;

    buf.take(script_len).read_to_end(&mut input.tx_in_script)?;

    input.sequence = buf.read_u32::<LittleEndian>()?;

    Ok(input)
}


pub fn parse_output(buf: &mut io::Cursor<&[u8]>) -> io::Result<Output> {

    let mut output: Output = Default::default();

    output.value = buf.read_u64::<LittleEndian>()?;

    let script_len = parse_varint(buf)?;

    buf.take(script_len).read_to_end(&mut output.tx_out_script)?;

    Ok(output)
}


impl fmt::Display for Transaction {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {

        write!(fmt, "Transaction\n\t- version: {}\n\t- flags: {:?}\n\n", self.version, self.flags)?;

        for input in &self.inputs {

            write!(fmt, "\tinput:\n{}\n", input)?;
        }

        for output in &self.outputs {

            write!(fmt, "\toutput:\n{}\n", output)?;
        }

        write!(fmt, "\tlock_time: {}\n", self.lock_time)
    }
}


impl fmt::Display for Input {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {

        write!(fmt, "\t- prev_tx_hash: {}\n", hex::encode(&self.prev_tx_hash))?;
        write!(fmt, "\t- prev_tx_out_index: {}\n", self.prev_tx_out_index)?;
        write!(fmt, "\t- tx_in_script: {}\n", hex::encode(&self.tx_in_script))?;
        write!(fmt, "\t- sequence: {}\n", self.sequence)
    }
}


impl fmt::Display for Output {

    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {

        write!(fmt, "\t- value: {}\n", self.value)?;
        write!(fmt, "\t- tx_out_script: {}\n", hex::encode(&self.tx_out_script))
    }
}


#[derive(Debug)]
pub enum OpCode {

    PASSTHRU,
    OP_PUSHDATA1,
    OP_DUP,
    OP_HASH160,
    OP_EQUALVERIFY(bool),
    OP_CHECKSIG
}


impl OpCode {

    pub fn exec(buf: &mut io::Cursor<&[u8]>, stack: &mut Vec<Vec<u8>>) -> io::Result<OpCode> {

        use OpCode::*;

        let mut pop = || stack.pop().ok_or(io::Error::new(io::ErrorKind::Other, "OP_DUP in empty stack"));

        let op = buf.read_u8()?;

        match op {

            0x01 ... 0x4b => {

                //The next opcode bytes is data to be pushed onto the stack

                let mut data = Vec::new();
                buf.take(op as u64).read_to_end(&mut data)?;

                stack.push(data);

                Ok(PASSTHRU)
            }

            0x4c => {

                // OP_PUSHDATA1: The next byte contains the number of bytes to be pushed onto the stack.

                let len = buf.read_u8()? as u64;

                let mut data = Vec::new();
                buf.take(len).read_to_end(&mut data)?;

                stack.push(data);

                Ok(OP_PUSHDATA1)
            }

            0x76 => {

                // OP_DUP: Duplicates the top stack item

                let item_n = pop()?;
                stack.push(item_n.clone());
                stack.push(item_n);

                Ok(OP_DUP)
            }

            0xa9 => {

                // OP_HASH160: The input is hashed twice: first with SHA-256 and then with RIPEMD-160.

                let item_n = pop()?;

                let mut sha256_hasher = Sha256::new();
                sha256_hasher.input(&item_n);

                let mut sha: [u8 ; 32] = [0 ; 32];
                sha256_hasher.result(&mut sha);

                let mut ripemd160_hasher = Ripemd160::new();
                ripemd160_hasher.input(&sha);

                let mut rip : [u8 ; 20] = [0 ; 20];
                ripemd160_hasher.result(&mut rip);

                stack.push(rip.to_vec());

                Ok(OP_HASH160)
            }

            0x88 => {

                // OP_EQUALVERIFY: Nothing / fail 	Same as OP_EQUAL, but runs OP_VERIFY afterward.
                let item_n = pop()?;
                let item_m = pop()?;

                if item_n == item_m {

                    Ok(OP_EQUALVERIFY(true))

                } else {

                    Ok(OP_EQUALVERIFY(false))
                }
            }

            0xac => {

                // OP_CHECKSIG: TODO: Pas demandé dans le cadre de l'exercice

                let _item_n = pop()?;
                let _item_m = pop()?;

                stack.push(vec![0x1]);
                Ok(OP_CHECKSIG)
            }

            _ => Err(io::Error::new(io::ErrorKind::Other, format!("Bad opcode {:02x}", op)))
        }
    }
}


pub fn print_stack(stack: &Vec<Vec<u8>>) {

    for (level, data) in stack.into_iter().enumerate() {

        println!("{}: len={} data={}", level, data.len(), hex::encode(data));
    }
}


#[cfg(test)]
mod tests {

    use crate::*;

    // Exercices 1.4.1 et 1.4.2 : Convertir un nombre décimal en hexadécimal
    #[test]
    fn decimal_vers_hexadecimal() {

        //0x 07 1d 91 (big endian) ou 00 07 1d 91
        //0x 91 1d 07 (little endian) ou 91 1d 07 00

        let (little, big, var) = u64_to_hex(466321);
        assert_eq!(little, "911d070000000000");
        assert_eq!(big, "0000000000071d91");
        assert_eq!(var, "fe911d0700");

        let (_, _, var) = u64_to_hex(106);
        assert_eq!(var, "6a");

        let (_, _, var) = u64_to_hex(550);
        assert_eq!(var, "fd2602");

        let (_, _, var) = u64_to_hex(998000);
        assert_eq!(var, "fe703a0f00");
    }


    // Exercice 1.4.3 : écrire un script (qui décode une entrée)
    #[test]
    fn parser_entree() {

        let payload = hex::decode(&"941e985075825e09de53b08cdd346bb67075ef0ce5c94f9\
8853292d4bf94c10d010000006b483045022100ab44ef425e6d85c03cf301bc16465e3176b55bba9727\
706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c030bf8ce596e692021b66441b39b4b3\
5e64e012102f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861a7fdb2fd577ed4\
8a81Feffffff"[..]).unwrap();

        let mut buf = io::Cursor::new(&payload[..]);

        let input = parse_input(&mut buf).unwrap();

        println!("{}", input);

//    - prev_tx_hash: 0dc194bfd4923285984fc9e50cef7570b66b34dd8cb053de095e827550981e94
//    - prev_tx_out_index: 1
//    - tx_in_script: 483045022100ab44ef425e6d85c03cf301bc16465e3176b55bba9727706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c030bf8ce596e692021b66441b39b4b35e64e012102f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861a7fdb2fd577ed48a81
//    - sequence: 4294967294

        let unread = buf.bytes().count();
        assert_eq!(unread, 0);

        assert_eq!(input.prev_tx_hash.to_vec(), hex::decode(&"0dc194bfd4923285984fc9e5\
0cef7570b66b34dd8cb053de095e827550981e94"[..]).unwrap());
        assert_eq!(input.prev_tx_out_index, 1);
        assert_eq!(input.tx_in_script.to_vec(), hex::decode(&"483045022100ab44ef425e6d\
85c03cf301bc16465e3176b55bba9727706819eaf07cf84cf52d02203f7dc7ae9ab36bead14dd3c83c8c03\
0bf8ce596e692021b66441b39b4b35e64e012102f63ae3eba460a8ed1be568b0c9a6c947abe9f079bcf861\
a7fdb2fd577ed48a81"[..]).unwrap());
        assert_eq!(input.sequence, 4294967294);
    }


    // Exercice 1.4.4 : écrire un script (qui décode la transaction)
    #[test]
    fn parser_transaction() {

        let payload = hex::decode(&b"0100000001f129de033c57582efb464e94ad438fff\
493cc4de4481729b85971236858275c2010000006a4730440220155a2ea4a702cadf37052c87bfe\
46f0bd24809759acff8d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b\
0c578133a7b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c6\
2d2c29ff6863d501affffffff02ccaec817000000001976a9142527ce7f0300330012d6f97672d9\
acb5130ec4f888ac18411a000000000017a9140b8372dffcb39943c7bfca84f9c40763b8fa9a068\
700000000"[..]).unwrap();

        let mut buf = io::Cursor::new(&payload[..]);

        let tx = parse_transaction(&mut buf).unwrap();

        println!("{}", tx);

// Transaction
//     - version: 1
//     - flags: None
//
//     input:
//     - prev_tx_hash: c2758285361297859b728144dec43c49ff8f43ad944e46fb2e58573c03de29f1
//     - prev_tx_out_index: 1
//     - tx_in_script: 4730440220155a2ea4a702cadf37052c87bfe46f0bd24809759acff8d8a7206979610e46f6022052b688b784fa1dcb1cffeef89e7486344b814b0c578133a7b0bce5be978a9208012103915170b588170cbcf6380ef701d19bd18a526611c0c69c62d2c29ff6863d501a
//     - sequence: 4294967295
//
//     output:
//     - value: 399027916
//     - tx_out_script: 76a9142527ce7f0300330012d6f97672d9acb5130ec4f888ac
//
//     output:
//     - value: 1720600
//     - tx_out_script: a9140b8372dffcb39943c7bfca84f9c40763b8fa9a0687
//
//     lock_time: 0

        let unread = buf.bytes().count();
        assert_eq!(unread, 0);

        assert_eq!(tx.version, 1);
        assert_eq!(tx.flags, None);

        assert_eq!(tx.inputs.len(), 1);

        assert_eq!(tx.inputs[0].prev_tx_hash.to_vec(), hex::decode(&"c275828536\
1297859b728144dec43c49ff8f43ad944e46fb2e58573c03de29f1"[..]).unwrap());
        assert_eq!(tx.inputs[0].prev_tx_out_index, 1);
        assert_eq!(tx.inputs[0].tx_in_script.to_vec(), hex::decode(&"4730440220\
155a2ea4a702cadf37052c87bfe46f0bd24809759acff8d8a7206979610e46f6022052b688b784f\
a1dcb1cffeef89e7486344b814b0c578133a7b0bce5be978a9208012103915170b588170cbcf638\
0ef701d19bd18a526611c0c69c62d2c29ff6863d501a"[..]).unwrap());
        assert_eq!(tx.inputs[0].sequence, 4294967295);

        assert_eq!(tx.outputs.len(), 2);

        assert_eq!(tx.outputs[0].tx_out_script.to_vec(), hex::decode(&"76a91425\
27ce7f0300330012d6f97672d9acb5130ec4f888ac"[..]).unwrap());
        assert_eq!(tx.outputs[0].value, 399027916);

        assert_eq!(tx.outputs[1].tx_out_script.to_vec(), hex::decode(&"a9140b83\
72dffcb39943c7bfca84f9c40763b8fa9a0687"[..]).unwrap());
        assert_eq!(tx.outputs[1].value, 1720600);

        assert_eq!(tx.lock_time, 0);
    }


    // Exercice 1.4.7 : Vérifier la validité d’une transaction Pay-to-pubkey-hash
    #[test]
    fn verifier_p2pkh() {

        let mut script_sig = hex::decode(&b"483045022100d544eb1ede691f9833d44e5\
266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3\
184d1994015aae43a228faa608362001210372cc7efb1961962bba20db0c6a3eebdde0ae606986b\
f76cb863fa460aee8475c"[..]).unwrap();

        let mut script_pub_key = hex::decode(&b"76a9147c3f2e0e3f3ec87981f9f2059\
537a355db03f9e888ac"[..]).unwrap();

        let mut payload = Vec::new();

        payload.append(&mut script_sig);
        payload.append(&mut script_pub_key);

        let mut buf = io::Cursor::new(&payload[..]);

        let mut stack = Vec::new();

        loop {

            let res = OpCode::exec(&mut buf, &mut stack);

            match res {

                Ok(OpCode::OP_EQUALVERIFY(false)) => { println!("Transaction is invalid"); break }

                Ok(op) => { println!("{:?} ok", op) }

                Err(ref e) if e.kind() == io::ErrorKind::UnexpectedEof => { println!("Done"); break }

                Err(e) => { println!("Fail with {}", e); break }
            }

            print_stack(&stack);
            println!("");
        }

    //    PASSTHRU ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //
    //    PASSTHRU ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //    1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //
    //    OP_DUP ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //    1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //    2: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //
    //    OP_HASH160 ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //    1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //    2: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8
    //
    //    PASSTHRU ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //    1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //    2: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8
    //    3: len=20 data=7c3f2e0e3f3ec87981f9f2059537a355db03f9e8
    //
    //    OP_EQUALVERIFY(true) ok
    //    0: len=72 data=3045022100d544eb1ede691f9833d44e5266e923dae058f702d2891e4ee87621a433ccdf4f022021e405c26b0483cd7c5636e4127a9510f3184d1994015aae43a228faa608362001
    //    1: len=33 data=0372cc7efb1961962bba20db0c6a3eebdde0ae606986bf76cb863fa460aee8475c
    //    OP_CHECKSIG ok
    //    0: len=1 data=01
    //
    //        Done

        assert_eq!(stack, vec![vec![1 as u8]]);
    }
}
