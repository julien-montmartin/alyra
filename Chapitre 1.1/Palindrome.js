function palindrome(s) {

     s = s.replace(/ /g, '').toLowerCase();

     var i = 0
     var j = s.length - 1;

     while (i < j) {

         if (s[i] != s[j]) {

             return false
         }
         i+=1
         j-=1
     }

    return true
}


var tests = ["aba", "abba", "toto", "ESOPE RESTE ICI ET SE repose"]


for (i in tests) {

	if (palindrome(tests[i])) {

		console.log(`C'est un palindrome : ${tests[i]}`)

	} else {

		console.log(`Ce n'est pas un palindrome : ${tests[i]}`)
	}
}
