#Écrire un programme qui étant donné un nombre saisi par l'utilisateur entre 1 et 100 recherche ce nombre de façon efficace.

#La valeur saisie par l’utilisateur est stockée dans une variable, par exemple solution.
#Le programme choisit une première valeur et l’enregistre dans une autre variable estimation les deux sont comparées…

#Idéalement la partie vérification et la partie recherche sont deux fonctions indépendantes.


import random


def check(n, N, min, max):

        if n < N:

            return (n, max)

        elif n > N:

            return (min, n)

        else:

            return (n, n)


def search(n, N, min, max):

    while min != max:

        print("Peut-être %d ?" % (n))

        (min, max) = check(n, N, min, max)

        middle = int(min + (max - min) / 2)

        if n == middle:

            n = middle + 1

        else:

            n = middle

    return min



print("Entrez N avec 1≤N≤100\n")

while True:

    N = input("Alors ?\n")

    try:
        N = int(N)

        if N < 1 or N > 100:

            print("On a dit entre 1 et 100 !")

        else:

            break

    except ValueError:

        print("Sérieusement ?")


n = random.randint(1,100)

r = search(n, N, 1, 100)

print("Trouvé %d !" % (r))


# Entrez N avec 1≤N≤100
#
# Alors ?
# 37
# Peut-être 21 ?
# Peut-être 60 ?
# Peut-être 40 ?
# Peut-être 30 ?
# Peut-être 35 ?
# Peut-être 37 ?
# Trouvé 37 !
#
# Process finished with exit code 0