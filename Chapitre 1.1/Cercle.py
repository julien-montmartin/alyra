# Construire une classe cercle :
# Son constructeur prend en paramètre un rayon
# Ses méthodes aire() et périmetre() retournent les valeurs correspondantes.


import math


class Shape:

    def __str__(self):

        return "This is a {} of perimeter {:0.2f} and area {:0.2f}".format(self.getName(), self.getPerimeter(), self.getArea())


class Circle(Shape):

    def __init__(self, r):

        super().__init__()

        self.r = r

    def getName(self):

        return "Circle"

    def getPerimeter(self):

        return 2 * math.pi * self.r

    def getArea(self):

        return math.pi * self.r ** 2


class Rectangle(Shape):

    def __init__(self, l, L):

        super().__init__()

        self.l = (l, L)[l < L]
        self.L = (l, L)[l > L]

    def getName(self):

        return "Rectangle"

    def getPerimeter(self):

        return 2 * (self.l + self.L)

    def getArea(self):

        return self.l * self.L


class Square(Rectangle):

    def __init__(self, c):

        super().__init__(c, c)

    def getName(self):

        return "Square"



c = Circle(3)
print(c)

r = Rectangle(2, 3)
print(r)

s = Square(2)
print(s)


# This is a Circle of perimeter 18.85 and area 28.27
# This is a Rectangle of perimeter 10.00 and area 6.00
# This is a Square of perimeter 8.00 and area 4.00