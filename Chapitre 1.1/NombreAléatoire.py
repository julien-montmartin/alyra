# Construire un jeu qui pour tire aléatoirement un nombre entre 1 et 100 et demande à l'utilisateur de le deviner.
# Pour chaque entrée de l'utilisateur le programme affiche si la bonne réponse est supérieure ou inférieure.
# Il affiche aussi si la réponse est très proche (<=5), proche ( 6 à 10) ou supérieur.


import random


#random.randint(a, b)
#   Return a random integer N such that a <= N <= b.
N = random.randint(1,100)

#print("Trouvez 1≤%d≤100\n" % (N))
print("Trouvez 1≤N≤100\n")

while True:

    guess = input("Alors ?\n")

    try:
        guess = int(guess)

        if guess < 1 or guess > 100:

            print("On a dit entre 1 et 100 !")
            continue

        hint = ""

        if abs(N-guess) <= 5:

            hint = "(mais très proche)"

        elif abs(N-guess) <= 10:

            hint = "(mais proche)"

        if N < guess:

            print("Le nombre à deviner est inférieur à %d %s" % (guess, hint))

        elif N > guess:

            print("Le nombre à deviner est supérieur à %d %s" %(guess, hint))

        else:

            print("Bravo, le nombre à deviner était bien %d !" % (N))
            break

    except ValueError:

        print("Sérieusement ?")


# Trouvez 1≤N≤100
#
# Alors ?
# 37
# Le nombre à deviner est supérieur à 37
# Alors ?
# 67
# Le nombre à deviner est supérieur à 67 (mais proche)
# Alors ?
# 74
# Le nombre à deviner est supérieur à 74 (mais très proche)
# Alors ?
# 75
# Le nombre à deviner est supérieur à 75 (mais très proche)
# Alors ?
# 76
# Le nombre à deviner est supérieur à 76 (mais très proche)
# Alors ?
# 77
# Bravo, le nombre à deviner était bien 77 !
#
# Process finished with exit code 0
