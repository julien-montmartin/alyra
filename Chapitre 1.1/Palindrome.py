# Écrire une fonction qui établit pour une chaîne de caractères donnée s'il s'agit d'un palindrome ou non.
#
# La fonction doit aussi vérifier les phrases en omettant les espaces.


def palindrome(s):

    s = s.replace(" ", "").lower()

    i = 0
    j = len(s) - 1

    while i < j:

        if s[i] != s[j]:

            return False

        i+=1
        j-=1

    return True


P = input("Entrez un palindrome (ou pas)\n")

if palindrome(P):

    print("C'est un palindrome !")

else:

    print("Ce n'est pas un palindrome !")


# Entrez un palindrome (ou pas)
# ESOPE RESTE ICI ET SE repose
# C'est un palindrome !
#
# Process finished with exit code 0