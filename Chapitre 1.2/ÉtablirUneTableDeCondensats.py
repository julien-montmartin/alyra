# Une entreprise dispose de serveurs répartis dans le monde et souhaite pouvoir facilement établir la correspondance
# entre chaque lieu et l’adresse IP de son serveur. Établissez une table de hachage de taille 8 à partir du tableau...


class Entry:

    def __init__(self, key, value):

        self.key = key
        self.value = value


class Node:

    def __init__(self, entry):

        self.entry = entry
        self.next = None


    def get_key(self):

        return self.entry.key


    def get_value(self):

        return self.entry.value


    def set_value(self, value):

        self.entry.value = value


class Bucket:

    def __init__(self, entry):

        self.head = None
        self.count = 0
        self.insert(entry)


    def insert(self, entry):

        node = self.get(entry.key)

        if node is not None:

            node.set_value(entry.value)
            return

        tail = self.head
        self.head = Node(entry)
        self.head.next = tail

        self.count += 1


    def get(self, key):

        node = self.head

        while node is not None and node.get_key() != key:

            node = node.next

        return node


class HashTable:

    def __init__(self, size):

        self.table = [None] * size

    def put(self, key, value):

        index = self.hash(key)
        entry = Entry(key, value)

        if self.table[index] is not None:

            self.table[index].insert(entry)

        else:

            self.table[index] = Bucket(entry)


    def get(self, key):

        index = self.hash(key)

        if self.table[index] is None or self.table[index].count == 0:

            return None

        else:

            node = self.table[index].get(key)

            if node is not None:
                return node.get_value()

            return None


    def hash(self, key):

        return hash(key) % len(self.table)


table = (("Amsterdam", "153.8.223.72"),
         ("Chennai", "169.38.84.49"),
         ("Dallas", "169.46.49.112"),
         ("Dallas, TX, USA", "184.173.213.155"),
         ("Frankfurt", "159.122.100.41"),
         ("Hong Kong", "119.81.134.212"),
         ("London", "5.10.5.200"),
         ("London", "158.176.81.249"),
         ("Melbourne", "168.1.168.251"),
         ("Mexico City", "169.57.7.230"),
         ("Milan", "159.122.142.111"),
         ("Paris", "159.8.78.42"),
         ("San Jose", "192.155.217.197"),
         ("São Paulo", "169.57.163.228"),
         ("Toronto", "169.56.184.72"),
         ("Washington DC", "50.87.60.166"))


ht = HashTable(8)

for (city, ip) in table:

    ips = ht.get(city)

    if ips is None:

        ht.put(city, [ip])

    else:

        ips.append(ip)


city_filter = HashTable(8)

for (city, xxx) in table:

    if city_filter.get(city) is None:

        city_filter.put(city, True)

        ips = ht.get(city)

        if len(ips) == 1:

            print("À {}, l'addresse du serveur est {}".format(city, ips))

        else:

            print("À {}, les addresses des serveurs sont {}".format(city, ips))

city = "tombouctou"
print("À {}, l'addresse du serveur est {}".format(city, ht.get(city)))


# À Amsterdam, l'addresse du serveur est ['153.8.223.72']
# À Chennai, l'addresse du serveur est ['169.38.84.49']
# À Dallas, l'addresse du serveur est ['169.46.49.112']
# À Dallas, TX, USA, l'addresse du serveur est ['184.173.213.155']
# À Frankfurt, l'addresse du serveur est ['159.122.100.41']
# À Hong Kong, l'addresse du serveur est ['119.81.134.212']
# À London, les addresses des serveurs sont ['5.10.5.200', '158.176.81.249']
# À Melbourne, l'addresse du serveur est ['168.1.168.251']
# À Mexico City, l'addresse du serveur est ['169.57.7.230']
# À Milan, l'addresse du serveur est ['159.122.142.111']
# À Paris, l'addresse du serveur est ['159.8.78.42']
# À San Jose, l'addresse du serveur est ['192.155.217.197']
# À São Paulo, l'addresse du serveur est ['169.57.163.228']
# À Toronto, l'addresse du serveur est ['169.56.184.72']
# À Washington DC, l'addresse du serveur est ['50.87.60.166']
# À tombouctou, l'addresse du serveur est None
#
# Process finished with exit code 0
