# Dans le langage de programmation de votre choix :
#
#   - Définir la méthode pour trouver une valeur donnée dans un arbre binaire de recherche
#
#   - Écrire la méthode pour afficher l’arbre selon un parcours infixe
#
#   - Écrire la méthode pour supprimer un nœud donné en distinguant trois cas :
#
#         - Le nœud est une feuille -> suppression simple
#
#         - Le nœud a un seul enfant -> il est remplacé par lui
#
#         - Le nœud à deux enfants, on le remplace alors par le nœud le plus proche, c’est à dire le nœud le plus à
#           droite de l’arbre gauche ou le plus à gauche de l'arbre droit.


class Nœud:

    def __init__(self, v):

        self.gauche = None
        self.droit = None
        self.valeur = v


class Arbre:

    def __init__(self):

        self.racine = None


    def ajouter(self, val):

        if self.racine is None:

            self.racine = Nœud(val)

        else:

            self._ajouter(val, self.racine)


    def _ajouter(self, val, nœud):

        if val < nœud.valeur:

            if nœud.gauche is not None:

                self._ajouter(val, nœud.gauche)

            else:

                nœud.gauche = Nœud(val)

        else:

            if nœud.droit is not None:

                self._ajouter(val, nœud.droit)

            else:

                nœud.droit = Nœud(val)


    def trouver(self, val):

        if self.racine is None:

            return None

        return self._trouver(val, self.racine)


    def _trouver(self, val, nœud):

        if val == nœud.valeur:
            return nœud

        if val < nœud.valeur:

            if nœud.gauche is None:

                return None

            return self._trouver(val, nœud.gauche)

        else:

            if nœud.droit is None:

                return None

            return self._trouver(val, nœud.droit)


    def affiche_infixe(self):

        if self.racine is not None:
            self._affiche_infixe(self.racine)


    def _affiche_infixe(self, nœud, profondeur=1):

        if nœud.gauche is not None:
            self._affiche_infixe(nœud.gauche, profondeur + 1)

        print("{} ".format(nœud.valeur), end='')

        if nœud.droit is not None:
            self._affiche_infixe(nœud.droit, profondeur + 1)


    def supprimer(self, val):

        if self.racine is not None:

            if self.racine.valeur != val:

                self._supprimer(None, self.racine, val)

            else:
                # C'est la racine qu'on veut supprimer, on traite ce cas à part car elle n'a pas de parent

                if self.racine.gauche is None and self.racine.droit is None:

                    self.racine = None

                elif self.racine.gauche is None and self.racine.droit is not None:

                    self.racine = self.racine.droit

                elif self.racine.gauche is not None and self.racine.droit is None:

                    self.racine = self.racine.gauche

                else:

                    # La racine a deux enfants, on prend arbitrairement le plus petit à droite
                    tmp = self.racine.droit

                    while tmp.gauche is not None:
                        tmp = tmp.gauche

                    self.racine.valeur = tmp.valeur
                    self._supprimer(self.racine, self.racine.droit, tmp.valeur)


    def _supprimer(self, parent, nœud, val):

        if nœud is None:
            return

        if val < nœud.valeur:

            self._supprimer(nœud, nœud.gauche, val)

        elif val > nœud.valeur:

            self._supprimer(nœud, nœud.droit, val)

        else:
            # On a trouvé le nœud

            if nœud.gauche is None and nœud.droit is None:

                if parent.valeur > nœud.valeur:

                    parent.gauche = None

                else:

                    parent.droit = None

            elif nœud.gauche is None and nœud.droit is not None:

                if parent.valeur > nœud.droit.valeur:

                    parent.gauche = nœud.droit

                else:

                    parent.droit = nœud.droit

            elif nœud.gauche is not None and nœud.droit is None:

                if parent.valeur > nœud.gauche.valeur:

                    parent.gauche = nœud.gauche

                else:

                    parent.droit = nœud.gauche

            else:

                # Le nœud a deux enfants, on prend arbitrairement le plus petit à droite
                tmp = nœud.droit

                while tmp.gauche is not None:
                    tmp = tmp.gauche

                nœud.valeur = tmp.valeur
                self._supprimer(nœud, nœud.droit, tmp.valeur)


a = Arbre()

for i in (24, 91, 70, 94, 25, 65, 28, 6, 90, 56, 75, 66):
    print("Ajout de {} : ".format(i), end='')
    a.ajouter(i)
    a.affiche_infixe()
    print("")

for i in (24, 91, 70, 94, 25, 65, 28, 6, 90, 56, 75, 66):
    print("Recherche de {} : {}".format(i, a.trouver(i).valeur))

woups = 1234

print("Recherche de {} : {}".format(woups, a.trouver(woups)))

for i in (1234, 94, 28, 56, 24, 90, 25, 65, 66, 70, 91, 6, 75):
    print("Suppression de {} : ".format(i), end='')
    a.supprimer(i)
    a.affiche_infixe()
    print("")


# Ajout de 24 : 24
# Ajout de 91 : 24 91
# Ajout de 70 : 24 70 91
# Ajout de 94 : 24 70 91 94
# Ajout de 25 : 24 25 70 91 94
# Ajout de 65 : 24 25 65 70 91 94
# Ajout de 28 : 24 25 28 65 70 91 94
# Ajout de 6 : 6 24 25 28 65 70 91 94
# Ajout de 90 : 6 24 25 28 65 70 90 91 94
# Ajout de 56 : 6 24 25 28 56 65 70 90 91 94
# Ajout de 75 : 6 24 25 28 56 65 70 75 90 91 94
# Ajout de 66 : 6 24 25 28 56 65 66 70 75 90 91 94
# Recherche de 24 : 24
# Recherche de 91 : 91
# Recherche de 70 : 70
# Recherche de 94 : 94
# Recherche de 25 : 25
# Recherche de 65 : 65
# Recherche de 28 : 28
# Recherche de 6 : 6
# Recherche de 90 : 90
# Recherche de 56 : 56
# Recherche de 75 : 75
# Recherche de 66 : 66
# Recherche de 1234 : None
# Suppression de 1234 : 6 24 25 28 56 65 66 70 75 90 91 94
# Suppression de 94 : 6 24 25 28 56 65 66 70 75 90 91
# Suppression de 28 : 6 24 25 56 65 66 70 75 90 91
# Suppression de 56 : 6 24 25 65 66 70 75 90 91
# Suppression de 24 : 6 25 65 66 70 75 90 91
# Suppression de 90 : 6 25 65 66 70 75 91
# Suppression de 25 : 6 65 66 70 75 91
# Suppression de 65 : 6 66 70 75 91
# Suppression de 66 : 6 70 75 91
# Suppression de 70 : 6 75 91
# Suppression de 91 : 6 75
# Suppression de 6 : 75
# Suppression de 75 :
#
# Process finished with exit code 0