use crypto::sha2::Sha256;
use crypto::digest::Digest;


pub struct MerkleTree {

    height: usize,  //Un u8 suffirait, mais ça évite des conversions...
    hashes: Vec<[u8 ; 32]>,
}


impl MerkleTree {


    pub fn new(values: &Vec<&[u8]>) -> Self {

        let mut capacity = 1;
        let mut height = 0;

        // On veut un arbre parfait, le nombre de feuilles nécessaires est la première puissance de deux supérieure
        // ou égale au nombre d'éléments à insérer
        while capacity < values.len() {

            capacity *= 2;
            height += 1;
        }

        // Ensuite on ajoute les noeuds intermédiares, jusqu'à la racine. On divise par deux à chaque étage
        let mut tmp_count = capacity;

        while tmp_count >= 1 {

            tmp_count = tmp_count / 2;
            capacity += tmp_count
        }

        // On sait maintenant comment dimensionner le vecteur, les hash inutilisés à la fin de ce dernier sont
        // arbitrairement mis à 0, ce qui permet à l'algo de fonctionner correctement
        let mut hashes = vec![[0 as u8; 32] ; capacity];


        // On hache les éléments afin de valoriser les feuilles de l'arbre
        let mut hasher = Sha256::new();

        for i in 0..values.len() {

            let node = (1<<height)-1+i;

            hasher.input(values[i]);
            hasher.result(&mut hashes[node]);
            hasher.reset();
        }

        // Puis on remonte dans l'arbre et on valorise les noeuds intermédiares en hachant les enfants
        for h in (0..height).rev() {

            for i in 0..1<<h {

                let node = (1<<h)-1+i;
                let left = (1<<h+1)-1+2*i;
                let right = left + 1;

                hasher.input(&hashes[left]);
                hasher.input(&hashes[right]);
                hasher.result(&mut hashes[node]);
                hasher.reset();
            }
        }

        // On garde la hauteur de l'arbre, ce qui facilite les calculs d'index par la suite
        MerkleTree { height, hashes }
    }


    pub fn root(&self) -> &[u8 ; 32] {

        &self.hashes[0]
    }


    pub fn proof(&self, value: &[u8]) -> bool {

        // On hache l'élément à chercher et on regarde s'il est présent dans les feuilles de l'arbre
        let mut hasher = Sha256::new();
        hasher.input(value);

        let mut hash = [0 as u8 ; 32];
        hasher.result(&mut hash);
        hasher.reset();

        let first_leaf = (1<<self.height)-1;
        let mut node = 0;

        for i in first_leaf..self.hashes.len() {

            if &hash == &self.hashes[i] {

                node = i;
                break;
            }
        }

        if node == 0 {

            println!{"✗ height=0 bad hash '{}' not found in tree leaves\n", hex::encode(&hash)};

            return false;
        }

        println!{"✓ height=0 found... '{}' in tree leaves", hex::encode(&hash)};

        // S'il est bien présent, on recalcule les hash en remontant pour s'assurer que l'arbre n'est pas corrompu

        for h in 0..self.height {

            let (_left, hash_left) = if node % 2 == 1 { (node, &self.hashes[node]) } else { (node - 1, &self.hashes[node - 1]) };
            let (right, hash_right) = if node % 2 == 0 { (node, &self.hashes[node]) } else { (node + 1, &self.hashes[node + 1]) };

            hasher.input(hash_left);
            hasher.input(hash_right);

            let mut hash = [0 as u8; 32];
            hasher.result(&mut hash);

            hasher.reset();

            println!{"# height={} found... '{}' = hash('{}' + '{}')", h + 1, hex::encode(&hash), hex::encode(&hash_left), hex::encode(&hash_right)};

            if hash != self.hashes[right / 2 - 1] {

                println!{"✗ height={} expected '{}'", h + 1, hex::encode(self.hashes[right / 2 - 1])};
                return false;
            }

            println!{"✓ height={} expected '{}'", h + 1, hex::encode(self.hashes[right / 2 - 1])};

            node = right / 2 - 1;
        }

        println!{""};

        true

    }

}


#[cfg(test)]
mod tests {

    use crate::MerkleTree;


    #[test]
    fn merkle_tree() {

        let vec_of_bytes = |v: Vec<&'static str>| {v.into_iter().map(|s| s.as_bytes()).collect::<Vec<&[u8]>>()};

        let mut set = vec_of_bytes(vec!["a", "b", "c"]);

        let t=MerkleTree::new(&set);

        assert_eq!(&hex::encode(t.root()), "d0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832");

        set.push("x".as_bytes());

        let res = set.into_iter().map(|x| t.proof(x)).collect::<Vec<bool>>();

        assert_eq!(res, vec![true, true, true, false]);

        println!("------------------------------------------------------------");

        let mut set = vec_of_bytes(vec!["a", "b", "c", "d"]);

        let t=MerkleTree::new(&set);

        assert_eq!(&hex::encode(t.root()), "14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7");

        set.push("x".as_bytes());

        let res = set.into_iter().map(|x| t.proof(x)).collect::<Vec<bool>>();

        assert_eq!(res, vec![true, true, true, true, false]);

        println!("------------------------------------------------------------");

        let mut set = vec_of_bytes(vec!["a", "b", "c", "d", "e"]);

        let t=MerkleTree::new(&set);

        assert_eq!(&hex::encode(t.root()), "c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c");

        set.push("x".as_bytes());

        let res = set.into_iter().map(|x| t.proof(x)).collect::<Vec<bool>>();

        assert_eq!(res, vec![true, true, true, true, true, false]);

        println!("------------------------------------------------------------");

        //    ✓ height=0 found... 'ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + '898184a7d6a032c38817722d914121832084222eb30943d7ae635fc479d1a859')
        //    ✓ height=2 expected 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832'
        //
        //    ✓ height=0 found... '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + '898184a7d6a032c38817722d914121832084222eb30943d7ae635fc479d1a859')
        //    ✓ height=2 expected 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832'
        //
        //    ✓ height=0 found... '2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' in tree leaves
        //    # height=1 found... '898184a7d6a032c38817722d914121832084222eb30943d7ae635fc479d1a859' = hash('2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' + '0000000000000000000000000000000000000000000000000000000000000000')
        //    ✓ height=1 expected '898184a7d6a032c38817722d914121832084222eb30943d7ae635fc479d1a859'
        //    # height=2 found... 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + '898184a7d6a032c38817722d914121832084222eb30943d7ae635fc479d1a859')
        //    ✓ height=2 expected 'd0a664079d491a97357efa1ce1eab5aeb566adef78a2b910e8d13e901e192832'
        //
        //    ✗ height=0 bad hash '2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881' not found in tree leaves
        //
        //        ------------------------------------------------------------
        //    ✓ height=0 found... 'ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //
        //    ✓ height=0 found... '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //
        //    ✓ height=0 found... '2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' in tree leaves
        //    # height=1 found... 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b' = hash('2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' + '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4')
        //    ✓ height=1 expected 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //
        //    ✓ height=0 found... '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4' in tree leaves
        //    # height=1 found... 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b' = hash('2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' + '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4')
        //    ✓ height=1 expected 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //
        //    ✗ height=0 bad hash '2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881' not found in tree leaves
        //
        //        ------------------------------------------------------------
        //    ✓ height=0 found... 'ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //    # height=3 found... 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c' = hash('14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' + '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca')
        //    ✓ height=3 expected 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c'
        //
        //    ✓ height=0 found... '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d' in tree leaves
        //    # height=1 found... 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' = hash('ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb' + '3e23e8160039594a33894f6564e1b1348bbd7a0088d42c4acb73eeaed59c009d')
        //    ✓ height=1 expected 'e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //    # height=3 found... 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c' = hash('14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' + '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca')
        //    ✓ height=3 expected 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c'
        //
        //    ✓ height=0 found... '2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' in tree leaves
        //    # height=1 found... 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b' = hash('2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' + '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4')
        //    ✓ height=1 expected 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //    # height=3 found... 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c' = hash('14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' + '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca')
        //    ✓ height=3 expected 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c'
        //
        //    ✓ height=0 found... '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4' in tree leaves
        //    # height=1 found... 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b' = hash('2e7d2c03a9507ae265ecf5b5356885a53393a2029d241394997265a1a25aefc6' + '18ac3e7343f016890c510e93f935261169d9e3f565436429830faf0934f4f8e4')
        //    ✓ height=1 expected 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b'
        //    # height=2 found... '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' = hash('e5a01fee14e0ed5c48714f22180f25ad8365b53f9779f79dc4a3d7e93963f94a' + 'bffe0b34dba16bc6fac17c08bac55d676cded5a4ade41fe2c9924a5dde8f3e5b')
        //    ✓ height=2 expected '14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7'
        //    # height=3 found... 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c' = hash('14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' + '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca')
        //    ✓ height=3 expected 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c'
        //
        //    ✓ height=0 found... '3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea' in tree leaves
        //    # height=1 found... 'f92051459290ef5b58b958b6dad86eb9cf147d27f2ef3825b5095b7c69d0c00b' = hash('3f79bb7b435b05321651daefd374cdc681dc06faa65e374e38337b88ca046dea' + '0000000000000000000000000000000000000000000000000000000000000000')
        //    ✓ height=1 expected 'f92051459290ef5b58b958b6dad86eb9cf147d27f2ef3825b5095b7c69d0c00b'
        //    # height=2 found... '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca' = hash('f92051459290ef5b58b958b6dad86eb9cf147d27f2ef3825b5095b7c69d0c00b' + 'f5a5fd42d16a20302798ef6ed309979b43003d2320d9f0e8ea9831a92759fb4b')
        //    ✓ height=2 expected '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca'
        //    # height=3 found... 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c' = hash('14ede5e8e97ad9372327728f5099b95604a39593cac3bd38a343ad76205213e7' + '28e3a58225433bf891be8b8c0bcc201e93e71cdb55ae7dfd3e804e9c86fd7bca')
        //    ✓ height=3 expected 'c6cde104e4847b9111f224882d4fb270b5f240f1bd24dda998828dc06303708c'
        //
        //    ✗ height=0 bad hash '2d711642b726b04401627ca9fbac32f5c8530fb1903cc4db02258717921a4881' not found in tree leaves
        //
        //        ------------------------------------------------------------
    }
}
