# Retourne la valeur du meilleur chargement possible pour les i premiers objets et le poids w
#
# s : la collection d'objets sous forme (poids, valeur)
# w : la charge à maximiser
# i : on considère les i premiers objets
# v[i][w] : la valeur du meilleur chargement possible avec les i premiers objets pour le poids w
# k[i][w] : True si l'objet i fait partie du meilleur chargement possible pour le poids w

def knapsack_rec(s, w, i, v, k):

    # Si on a déjà déterminé la valeur du meilleur chargement possible pour la capacité W, c'est fini !
    if v[i][w] > 0:

        return v[i][w]

    # Si on ne met rien dans le sac, la valeur du meilleur chargement est 0, c'est fini !
    if i == 0:

        return 0

    # Poids et valeur de l'objet i
    pi = s[i - 1][0]
    vi = s[i - 1][1]

    # Si l'objet i est à lui tout seul trop lourd pour la capacité restante, on le laisse, et on passe au suivant
    if pi > w:

        v[i][w] = knapsack_rec(s, w, i - 1, v, k)
        return v[i][w]

    else:

        # Première alternative, on explore en laissant l'objet i
        v1 = knapsack_rec(s, w, i - 1, v, k)

        # Deuxième alternative, on le prend, et la valeur du meilleur chargement est alors la somme de la valeur de
        # l'objet i et du chargement optimal pour un poids diminué de pi des objets restants
        v2 = vi + knapsack_rec(s, w - pi, i - 1, v, k)

        # On peut maintenant mettre à jour la valeur du meilleur chargement connu pour le poids W.
        # En passant, si l'objet i a été retenu, on le note dans K
        if v1 > v2:

            v[i][w] = v1

        else:

            v[i][w] = v2
            k[i][w] = True

        return v[i][w]


# Retourne la valeur du meilleur chargement possible ainsi que la liste des objets
#
# s : la collection d'objets sous forme (poids, valeur)
# w : la charge à maximiser

def knapsack(s, w):

    # Le nombre d'objets dans S
    n = len(s)

    # v[i][w] : la valeur du meilleur chargement possible avec les i premiers objets pour un poids w
    # k[i][w] : dit si l'on garde l'objet i pour le meilleur chargement de poids w
    # (Générateurs créant une liste de n éléments, dont chacun est une liste de W zéros/False)
    v = [[0 for i in range(w + 1)] for j in range(n + 1)]
    k = [[False for i in range(w + 1)] for j in range(n + 1)]

    # On calcule la valeur du meilleur chargement
    best_value = knapsack_rec(s, w, len(s), v, k)

    # Il faut maintenant reconstruire la liste des objets qu'on a mis dans le sac
    best_items = []
    j = w

    # La table k est construite lorsque la récursion remonte. Les premières lignes de k sont donc construites en ne
    # considérant que les premiers objets. Lorsqu'on arrive à la dernière ligne, tous les objets ont bien été pris en
    # compte. C'est donc par la dernière ligne qu'on reconstruit la liste des objets à mettre dans le sac. Et par la
    # dernière colonne puisqu'on s'intéresse au meilleur chargement du sac.

    for i in range(n, 0, -1):

        # Si l'objet i fait partie du meilleur chargement pour le poids j, on l'ajoute à la liste et on continue en
        # regardant si l'objet précédent fait partie du meilleur chargement avec le poids mis à jour.
        if k[i][j]:

            # On s'est simplifié la vie en indexant v et k à partir de 1 et non de 0, mais ce n'est pas l'usage...
            # On décale donc les indexs pour rendre une liste cohérente avec S.
            best_items.append(i - 1)
            pi = s[i - 1][0]
            j -= pi

    return best_value, best_items


# S : la collection d'objets sous forme (poids, valeur)
# W : le poids maximal supporté par le sac à dos
# S = ((12, 4), (1, 2), (4, 10), (1, 1), (2, 2))
# W = 15
# print(knapsack(S, W))
# (15, [4, 3, 2, 1])

# S = ((5, 10), (4, 40), (6, 30), (3, 50))
# W = 10
# print(knapsack(S, W))
# (90, [3, 1])

S = ((2000, 13000), (6000, 9000), (800, 2000), (700, 1500), (1200, 3500), (1000, 2800), (1300, 5000), (600, 1500))
W = 6000
print(knapsack(S, W))
# (25000, [7, 6, 4, 2, 0])

# Complexité de l'algo naïf : O(2^n), où n est le nombre d'objets (on construit récursivement un arbre de hauteur n où
# on a pour chaque noeud deux alternatives : on prend l'objet ou pas).