# On souhaite calculer factoriel d'un nombre a. Factoriel s'exprime comme le produit des entiers jusqu'à a inclus : a! = 1 x 2 x 3 ... x a
# Définir un programme pour calculer la factorielle d'un nombre a . Quel est le nombre d'opérations à effectuer?


def Factorielle(n):

    if n == 0:

        return 1

    else:

        return n*Factorielle(n - 1)


for n in range(1, 10):

    print("Factorielle({})={}".format(n, Factorielle(n)))


# La complexité de Factorielle(n) est linéaire, cad en O(n)


# Factorielle(1)=1
# Factorielle(2)=2
# Factorielle(3)=6
# Factorielle(4)=24
# Factorielle(5)=120
# Factorielle(6)=720
# Factorielle(7)=5040
# Factorielle(8)=40320
# Factorielle(9)=362880
