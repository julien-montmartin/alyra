pragma solidity ^0.5.7;


contract Assemblee {

    address[] membres;
    address[] admins;

    struct Decision {

        string description;
        uint votesPour;
        uint votesContre;
        mapping (address => bool) aVote;
    }

    Decision[] decisions;

    string public nomAssemblee;


    constructor(string memory nom) public {

        nomAssemblee = nom;

        admins.push(msg.sender);
    }


    function rejoindre() public {

        membres.push(msg.sender);
    }


    function estMembre(address utilisateur) public view returns (bool) {

        for (uint i=0 ; i<membres.length ; i++) {

            if (membres[i]==utilisateur) {

                return true;
            }
        }

        return false;
    }


    function proposerDecision(string memory description) public {

        require(estMembre(msg.sender));

        decisions.push(Decision(description, 0, 0));
    }


    function voter(uint vote,bool sens) public {

        require(vote < decisions.length);

        require(estMembre(msg.sender));

        require(decisions[vote].aVote[msg.sender] == false);

        if (sens == true) {

            decisions[vote].votesPour += 1;

        } else {

            decisions[vote].votesContre += 1;
        }

        decisions[vote].aVote[msg.sender] = true;
    }


    function comptabiliser(uint vote) public view returns (int) {

        require(vote < decisions.length);

        return int(decisions[vote].votesPour - decisions[vote].votesContre);
    }


    function estAdmin(address admin) public view returns (bool) {

        for (uint i=0 ; i<admins.length ; i++) {

            if (admins[i]==admin) {

                return true;
            }
        }

        return false;
    }


    function nommerAdmin(address admin) public {

        require(estAdmin(msg.sender));

        require(estAdmin(admin) == false);

        admins.push(admin);
    }


    function remercierAdmin(address admin) public {

        require(estAdmin(msg.sender));

        require(admins.length > 1);

        for (uint i=0 ; i<admins.length ; i++) {

            if (admins[i]==admin) {

				admins[i] = admins[admins.length - 1];
				admins.length--;
                break;
            }
        }
    }


    function fermerDecision(uint vote) public {

        require(vote < decisions.length);

        require(estAdmin(msg.sender));

		decisions[vote] = decisions[decisions.length - 1];
    }

}
