pragma solidity ^0.5.7;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract Credibilite {
  
    using SafeMath for uint256;
  
    mapping (address => uint256) public cred;
   
    bytes32[] private devoirs;
    
    event Transfer(address payeur, address destinataire, uint256 montant);

    event Devoir(bytes32 dev, address adr);
    

    function produireHash(string memory url) public pure returns (bytes32){
    
        return keccak256(bytes(url));
    }
    
    
    function transfer(address destinataire, uint256 valeur) public {
    
        require(cred[msg.sender] > valeur);
        require(cred[destinataire] > 0);
       
        cred[msg.sender] = cred[msg.sender].sub(valeur);
        cred[destinataire] = cred[destinataire].add(valeur);
        
        emit Transfer(msg.sender, destinataire, valeur);
    }
    
    
    function remettre(bytes32 dev) public returns (uint) {

        devoirs.push(dev);
       
        uint ordre = devoirs.length;
       
        uint valeur = 10;
       
        if(ordre < 3) {
       
            if (ordre == 1) { 
        
                // Bonus pour le premier à remettre son devoir
                valeur = 30;
    
            } else {
            
                // Bonus pour le deuxième à remettre son devoir
                valeur = 20;
            }
        }
        
        cred[msg.sender] = cred[msg.sender].add(valeur);

        emit Devoir(dev, msg.sender);

        return ordre;
    }
}

// Déployé à l'adresse 0xFFE4A38E72ED2CbaCDCf7eCD08bcC0ac147D6455